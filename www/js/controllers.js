angular.module('eventsApp.controllers', ['ngCordova', 'ionic'])

.controller('LoginCtrl', function($scope, $location, $http, $httpParamSerializer, RestService, $cordovaSQLite, $rootScope, $filter) {
    
    $scope.login = {
        username: null,
        password: null
    };
    
    $scope.nuevoUsuario = function() {
        $location.path('/nuevo-usuario');
    };
    $scope.cerrarAplicacion = function() {
        ionic.Platform.exitApp();
    };
    $scope.configuraciones = function () {
        $location.path('/configuraciones');
    };
    document.addEventListener('deviceready', function() {
        var db = window.sqlitePlugin.openDatabase({
            name: 'my.db',
            location: 'default'
        });
    
        $scope.authenticate = function() {

            RestService.postAuthorization($scope.login.username, $scope.login.password).then(
            function (response) {

                if (response.data['respuesta'] > 0){
                    
                    $rootScope.login = $scope.login.username;
                    $rootScope.loginId = response.data['respuesta'];
                    
                    $cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS TipoControl');
                    $cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS TiposPredio');
                    $cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS Usuarios');
                    $cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS Departamentos');
                    $cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS Distritos');
                    $cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS Barrios');


                    $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS TipoControl (id integer primary key autoincrement, nombre text)');

                    $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS TiposPredio (id integer primary key autoincrement, nombre text)');

                    $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS Usuarios (id integer primary key autoincrement, cedula text, nombre text, apellido text)')

                    $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS Departamentos (id integer primary key autoincrement, nombre text)');

                    $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS Distritos (id integer primary key autoincrement, id_departamento integer, nombre text, FOREIGN KEY(id_departamento) REFERENCES Departamentos(id))');

                    $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS Barrios (id integer primary key autoincrement, id_distrito integer, nombre text, FOREIGN KEY(id_distrito) REFERENCES Distritos(id))');
                    
                    RestService.listarTiposControles().then(
                    function (res) {

                        if (res.data.length > 0) {

                            for (var i = 0; i < res.data.length; i++) {                          
                                var query = 'INSERT INTO TipoControl (id, nombre) VALUES (?,?)';
                                $cordovaSQLite.execute(db, query, [res.data[i].id, res.data[i].nombre]).then(
                                function(res) {

                                }, function(err) {
                                    alert('Error TipoControl: ' + JSON.stringify(err));
                                });                          
                            }

                        } else {
                            alert('No se encontraron Tipos de Control');
                        }
                    }, function (err) {
                        alert('Error Lista Tipo Control: ' + err);
                    });
                    
                    RestService.listarDepartamentos().then(
                    function (res) {

                        if (res.data.length > 0) {
                            for (var i = 0; i < res.data.length; i++) {
                                var query = 'INSERT INTO Departamentos (id, nombre) VALUES (?,?)';
                                $cordovaSQLite.execute(db, query, [res.data[i].id, res.data[i].nombre]).then(
                                function(res) {

                                }, function(err) {
                                    alert('Error Departamentos: ' + JSON.stringify(err));
                                }); 
                            }

                        } else {
                            alert('No se encontraron Departamentos');
                        }
                    }, function (err) {
                        alert('Error Lista Departamentos: ' + err);
                    });
                    
                    RestService.listarDistritos(0).then(
                    function (res) {

                        if (res.data.length > 0) {
                            for (var i = 0; i < res.data.length; i++) {
                                var query = 'INSERT INTO Distritos (id, nombre, id_departamento) VALUES (?,?,?)';
                                $cordovaSQLite.execute(db, query, [res.data[i].id, res.data[i].nombre, res.data[i].fk_departamento]).then(
                                function(res) {

                                }, function(err) {
                                    alert('Error Distritos: ' + JSON.stringify(err));
                                });
                            }


                        } else {
                            alert('No se encontraron Distritos');
                        }
                    }, function (err) {
                        alert('Error Lista Distrito: ' + err);
                    });
                    
                    RestService.listarBarrios(0).then(
                    function (res) {

                        if (res.data.length > 0) {
                            for (var i = 0; i < res.data.length; i++) {
                                var query = 'INSERT INTO Barrios (id, nombre, id_distrito) VALUES (?,?,?)';
                                $cordovaSQLite.execute(db, query, [res.data[i].id, res.data[i].nombre, res.data[i].fk_distrito]).then(
                                function(res) {

                                }, function(err) {
                                    alert('Error Barrios: ' + JSON.stringify(err));
                                });
                            }

                        } else {
                            alert('No se encontraron Barrios');
                        }
                    }, function (err) {
                        alert('Error Lista Bario: ' + err);
                    });
                    
                    RestService.listarUsuarios().then(
                    function (res) {
                        $scope.list_usuario = [];

                        if (res.data.length > 0) {
                            for (var i = 0; i < res.data.length; i++) {
                                
                                var query = 'INSERT INTO Usuarios (id, cedula, nombre, apellido) VALUES (?,?,?,?)';
                                $cordovaSQLite.execute(db, query, [res.data[i].id, res.data[i].id_usuario, res.data[i].nombre, res.data[i].apellido]).then(
                                function(res) {

                                }, function(err) {
                                    alert('Error Usuarios: ' + JSON.stringify(err));
                                });
                                
                                $scope.list_usuario.push(res.data[i]);

                                if (res.data[i].id == $scope.planilla.encargado) {
                                    $scope.planilla.encargado = $scope.res.data[i];
                                }
                                if (res.data[i].id == $scope.planilla.operador) {
                                    $scope.planilla.operador = $scope.res.data[i];
                                }
                                if (res.data[i].id == $scope.planilla.supervisor) {
                                    $scope.planilla.supervisor = res.data[i];
                                }
                                if (res.data[i].id == $scope.planilla.usuario) {
                                    $scope.planilla.usuario = res.data[i];
                                }

                            }

                        } else {
                            alert('No se encontraron Usuarios');
                        }
                    }, function (err) {
                        alert('Error Lista Usuario: ' + err);
                    });
                    
                    RestService.listarTiposPredios().then(
                    function (res) {

                        if (res.data.length > 0) {
                            for (var i = 0; i < res.data.length; i++) {
                                var query = 'INSERT INTO TiposPredio (id, nombre) VALUES (?,?)';
                                $cordovaSQLite.execute(db, query, [res.data[i].id, res.data[i].nombre]).then(
                                function(res) {

                                }, function(err) {
                                    alert('Error TiposPredio: ' + JSON.stringify(err));
                                });
                            }

                        } else {
                            alert('No se encontraron Tipos de Predio');
                        }
                    }, function (err) {
                        alert('Error Lista Tipos Predios: ' + err);
                    });
                    
                    setTimeout(function() {$location.path('/tab/operaciones')}, 500);
                }
                else{                        
                    alert("Login Incorrecto");
                }
            }, function (err) {
                alert( "error: " + err);
            })


        };
        
    }, false);
    
})

.controller('ListCtrl', function($scope, EventsService, RecordsService) {
    $scope.events = EventsService.all();
    $scope.records = RecordsService.all();
})

.controller('AccountCtrl', function($scope, $rootScope, $location,
    $cordovaGeolocation, $cordovaFile,
    $cordovaNetwork, $cordovaSQLite) {
    $scope.logout = function() {
        $rootScope.loginId = null;
        $rootScope.login = null;
        $location.path('/login');
    };
    $scope.cambiarContrasena = function() {
        $location.path('/cambiar-contrasena');
    };
    
    document.addEventListener('deviceready', function() {
        $scope.obtenerCoordenadas = function() {
            var posOptions = {
                timeout: 10000,
                maximumAge: 0,
                enableHighAccuracy: true
            };

            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                .then(function(position) {

                    var latitud = position.coords.latitude;
                    var longitud = position.coords.longitude;
                    var precision = position.coords.accuracy;
                    var altitud = position.coords.altitude;
                    var timestamp = new Date(position.timestamp);

                    alert('latitud: ' + latitud + ' longitud: ' + longitud);
                    alert('precision: ' + precision + ' altitud: ' + altitud);
                    alert('Timestamp: ' + timestamp);
                }, function(err) {
                    alert('error: ' + err);
                });
        };

        $scope.escribirArchivo = function() {
            var textoJSON = {
                'nombre': 'juan',
                'apellido': 'perez'
            };
            $cordovaFile.writeFile(cordova.file.externalDataDirectory, 'mi-archivo.txt', JSON.stringify(textoJSON), true)
                .then(function(success) {
                    alert('exito');
                }, function(error) {
                    alert('error');
                });
        };

        $scope.leerArchivo = function() {
            $cordovaFile.readAsText(cordova.file.externalDataDirectory, 'mi-archivo.txt')
                .then(function(success) {
                    var resultado = JSON.parse(success);
                    alert('nombre = ' + resultado.nombre + ' apellido = ' + resultado.apellido);
                }, function(error) {
                    alert('error');
                });
        };

        /*var type = $cordovaNetwork.getNetwork();
        var isOnline = $cordovaNetwork.isOnline();
        var isOffline = $cordovaNetwork.isOffline();*/

        // listen for Online event
        /*$rootScope.$on('$cordovaNetwork:online', function(event, networkState) {
            var onlineState = networkState;
            alert('Estoy en linea ' + onlineState);
        })*/

        // listen for Offline event
        /*$rootScope.$on('$cordovaNetwork:offline', function(event, networkState) {
            var offlineState = networkState;
            alert('Estoy fuera de linea ' + offlineState);
        })*/

        var db = window.sqlitePlugin.openDatabase({
            name: 'my.db',
            location: 'default'
        });
        $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS people (id integer primary key, firstname text, lastname text)');

        $scope.insert = function(firstname, lastname) {
            var query = 'INSERT INTO people (firstname, lastname) VALUES (?,?)';
            $cordovaSQLite.execute(db, query, [firstname, lastname]).then(function(res) {
                alert('INSERT ID -> ' + res.insertId);
            }, function(err) {
                alert(err);
            });
        }

        $scope.select = function(lastname) {
            var query = 'SELECT firstname, lastname FROM people WHERE lastname = ?';
            $cordovaSQLite.execute(db, query, [lastname]).then(function(res) {
                if (res.rows.length > 0) {
                    alert('SELECTED -> ' + res.rows.item(0).firstname + ' ' + res.rows.item(0).lastname);
                } else {
                    alert('No results found');
                }
            }, function(err) {
                alert(err);
            });
        }

        $scope.eliminarDB = function() {
            window.sqlitePlugin.deleteDatabase({
                name: 'my.db',
                location: 'default'
            }, function() {
                alert('Successfully deleted database');
            }, function() {
                alert('Error while delete database');
            });
        }

        $scope.insertarRegistro = function() {
            $scope.insert('juan', 'perez');
        }

        $scope.leerRegistro = function() {
            $scope.select('perez');
        }
    }, false);
})

.controller('ConfiguracionesCtrl', function($scope, $location, $ionicPopup, RestService, 
    $rootScope, $cordovaSQLite) {

    document.addEventListener('deviceready', function() {
        /*var db = window.sqlitePlugin.openDatabase({
            name: 'my.db',
            location: 'default'
        });*/
        
        $scope.configuracion = {
            url: null,
            other: null
        };
        $scope.configuracion.url = 'http://192.168.43.227:8080';
        
        // An alert dialog
        $scope.showAlert = function() {
            var alertPopup = $ionicPopup.alert({
                title: 'Info',
                template: 'La operación se realizó con éxito'
            });

            alertPopup.then(function(res) {
                $location.path('/login');
            });
        };
        
        $scope.guardar = function () {

        };

        $scope.showConfirm = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Cancelar',
                template: 'Desea cancelar la operación?'
            });

            confirmPopup.then(function(res) {
                if (res) {
                    $scope.cancelar();
                }
            });
        };
        
        $scope.cancelar = function() {
            $location.path('/login');
        };
        
    }, false);
})

.controller('CambiarContrasenaCtrl', function($scope, $location, $ionicPopup, RestService, $rootScope, $ionicPlatform, $state ) {
    
    $scope.password = {
        login: null,
        actual: null,
        nuevo1: null,
        nuevo2: null
    };
    // An alert dialog
    $scope.showAlert = function() {
        var alertPopup = $ionicPopup.alert({
            title: 'Info',
            template: 'La operación se realizó con éxito'
        });

        alertPopup.then(function(res) {
            $location.path('/tab/account');
        });
    };
    $scope.guardar = function() {
        $scope.password.login = $rootScope.login;
        RestService.cambiarContrasena($scope.password).then(
            function (response) {
                if( response.data['respuesta'] == "OK"){
                    $scope.showAlert();   
                }
                else{
                    alert(response.data['respuesta']);         
                }

            }, function (err) {
                alert( "error: " + JSON.stringify(err));
            })
    };
    
    $scope.showConfirm = function() {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Cancelar',
            template: 'Desea cancelar la operación?'
        });

        confirmPopup.then(function(res) {
            if (res) {
                $scope.cancelar();
            }
        });
    };
    $scope.cancelar = function() {
        $location.path('/tab/account');
    };
    
    $ionicPlatform.registerBackButtonAction(function() {
        //var path = $location.path()
        //alert("state: " + $state.current.name);
        if ($state.current.name === "cambiar-contrasena") {
            //navigator.app.exitApp();
            $scope.showConfirm();       
        }
    }, 100);
    
})

.controller('NuevoUsuarioCtrl', function($scope, $location, $ionicPopup, RestService) {

        $scope.user = {
            id: null,
            id_usuario: null,
            contrasena: null,
            contrasena2: null,
            nombre: null,
            apellido: null,     
            fecha_nacimiento: null
        };
        
        // An alert dialog
        $scope.showAlert = function() {
            var alertPopup = $ionicPopup.alert({
                title: 'Info',
                template: 'La operación se realizó con éxito'
            });

            alertPopup.then(function(res) {
                $location.path('/login');
            });
        };
        $scope.guardar = function() {

            RestService.postCrearUsuario($scope.user).then(
            function (response) {
                if( response.data['respuesta'] == "OK"){
                    $scope.showAlert();   
                }
                alert(response.data['respuesta']);
            }, function (err) {
                alert( "error: " + err);
            })
            
            
        };
        $scope.cancelar = function() {
            $location.path('/login');
        };
    
        $scope.showConfirm = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Cancelar',
                template: 'Desea cancelar la operación?'
            });

            confirmPopup.then(function(res) {
                if (res) {
                    $scope.cancelar();
                }
            });
        };
    
    })
.controller('OperacionesCtrl', function($scope, $location, $ionicPopup, EventsService, $rootScope, RecordsService, $cordovaSQLite, RestService, $cordovaNetwork, $filter, $q, $ionicPlatform, $cordovaGeolocation, $ionicHistory) {

        document.addEventListener('deviceready', function() {
            var db = window.sqlitePlugin.openDatabase({
                name: 'my.db',
                location: 'default'
            });
            
            $scope.bloqueo = true;
            
            $rootScope.planillaViewId = $ionicHistory.currentHistoryId();
            
            /*$scope.isGPSDeviceReady = function() {
                if (window.cordova) {
                    cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
                        //alert("Location is " + (enabled ? "enabled" : "disabled"));
                        if(enabled == false){
                            alert("Active el GPS");
                        }
                    }, function(error) {
                        alert("The following error occurred: " + error);
                    });
                }
                setTimeout(function() {$scope.isGPSDeviceReady();}, 60000); 
            };
            $scope.isGPSDeviceReady();*/
            
            $scope.obtenerPlanillas = function() {
                
                var query = 'SELECT id, id_server, tipo_control, departamento, distrito, barrio, encargado, supervisor, operador, cuadrilla, brigada, usuario, fecha, estado, enviado FROM S1';
            
                var promesa = $cordovaSQLite.execute(db, query, []).then(
                function(res) {
                    for (var i = 0; i < res.rows.length; i++) {
                        alert('item: ' + JSON.stringify(res.rows.item(i)));    
                    }
                    
                }, function(err) {
                    alert('Error hay planillas para barrer: ' + JSON.stringify(err));
                    return false;
                });
                
                return promesa;
            };
            
            $scope.obtenerRegistros = function() {
                /*var query = 'SELECT r.id, r.id_server, r.id_S1, s.id_server AS id_S1_server, r.manzana, r.direccion, r.latitude, r.longitude, r.verificado, r.predio, r.jefe_familia, r.habitantes, r.febriles, r.sin_criaderos, r.criaderos_eliminados, r.criaderos_tratados_fisicamente, r.criaderos_tratados_larvicida, r.larvicida_A, r.larvicida_B, r.larvicida_C, r.estado, r.fecha, r.enviado, cA.criaderos_A1, cA.criaderos_A2, cA.criaderos_A3, cA.criaderos_A4, cA.criaderos_A5, cA.criaderos_A6, cA.criaderos_A7, cA.criaderos_A8, cA.criaderos_A9, cA.criaderos_A10, cB.criaderos_B1, cB.criaderos_B2, cB.criaderos_B3, cB.criaderos_B4, cB.criaderos_B5, cB.criaderos_B6, cC.criaderos_C1, cC.criaderos_C2, cC.criaderos_C3, cC.criaderos_C4 FROM RegistrosS1 AS r JOIN S1 AS s ON r.id_S1 = s.id LEFT OUTER JOIN CriaderosA AS cA ON r.id = cA.id_registro_S1 LEFT OUTER JOIN CriaderosB AS cB ON r.id = cB.id_registro_S1 LEFT OUTER JOIN CriaderosC AS cC ON r.id = cC.id_registro_S1';*/
                
                var query = 'SELECT r.id, r.id_server, r.id_S1, s.id_server AS id_S1_server, r.manzana, r.direccion, r.latitude, r.longitude, r.verificado, r.predio, r.jefe_familia, r.habitantes, r.febriles, r.sin_criaderos, r.criaderos_eliminados, r.criaderos_tratados_fisicamente, r.criaderos_tratados_larvicida, r.larvicida_A, r.estado, r.fecha, r.enviado, cA.criaderos_A1, cA.criaderos_A2, cA.criaderos_A3, cA.criaderos_A4, cA.criaderos_A5, cA.criaderos_A6, cA.criaderos_A7, cA.criaderos_A8, cA.criaderos_A9, cA.criaderos_A10, cB.criaderos_B1, cB.criaderos_B2, cB.criaderos_B3, cB.criaderos_B4, cB.criaderos_B5, cB.criaderos_B6, cC.criaderos_C1, cC.criaderos_C2, cC.criaderos_C3, cC.criaderos_C4 FROM RegistrosS1 AS r JOIN S1 AS s ON r.id_S1 = s.id LEFT OUTER JOIN CriaderosA AS cA ON r.id = cA.id_registro_S1 LEFT OUTER JOIN CriaderosB AS cB ON r.id = cB.id_registro_S1 LEFT OUTER JOIN CriaderosC AS cC ON r.id = cC.id_registro_S1';

                var promesa = $cordovaSQLite.execute(db, query, []).then(
                function(res) {
                    for (var i = 0; i < res.rows.length; i++) {
                        alert('item: ' + JSON.stringify(res.rows.item(i)));    
                    }
                    
                }, function(err) {
                    alert('Error hay registros para barrer: ' + JSON.stringify(err));
                });
                
                return promesa;
            };
            
            $scope.barrido = function() {                
                    
                setTimeout(function() {$scope.barrerPlanillas();}, 5000);
                //setTimeout(function() {$scope.obtenerPlanillas();}, 5000);
                    
                setTimeout(function() {$scope.barrerRegistros();}, 10000);
                //setTimeout(function() {$scope.obtenerRegistros();}, 10000);

            };
            
            // listen for Online event
            $rootScope.$on('$cordovaNetwork:online', function(event, networkState) {
                //$scope.isGPSDeviceReady();
                if($scope.bloqueo) return;

                var onlineState = networkState;
                //alert('Estoy en linea ' + onlineState);
                
                $scope.bloqueo = true;
                $scope.barrido();
                
            });
            
            // listen for Offline event
            $rootScope.$on('$cordovaNetwork:offline', function(event, networkState) {
                //$scope.isGPSDeviceReady();
                if(!$scope.bloqueo) return;
  
                var onlineState = networkState;
                //alert('Estoy fuera de linea ' + onlineState);
                
                $scope.bloqueo = false;
                
            });
            
            $scope.barrerPlanillas = function() {
                
                var query = 'SELECT id, id_server, tipo_control, departamento, distrito, barrio, encargado, supervisor, operador, cuadrilla, brigada, usuario, fecha, estado, enviado FROM S1 WHERE estado = ? and enviado = ?';

                $cordovaSQLite.execute(db, query, ["ACTIVO", false]).then(
                function(res) {
                    //alert('Lista planillas S1: ' + JSON.stringify(res));
                    if (res.rows.length > 0) {
                        for (var i = 0; i < res.rows.length; i++) {
                            //alert('item: ' + JSON.stringify(res.rows.item(i)));
                            var tmp = res.rows.item(i);
                            var planilla = {
                                tipo_control: {id:null, nombre:null},
                                departamento: {id:null, nombre:null},
                                distrito: {id:null, nombre:null},
                                barrio: {id:null, nombre:null},
                                encargado: {id:null, nombre:null},
                                operador: {id:null, nombre:null},
                                supervisor: {id:null, nombre:null},
                                cuadrilla: tmp.cuadrilla,
                                brigada: tmp.brigada,
                                usuario: tmp.usuario
                            };
                            planilla.id = tmp.id;
                            planilla.tipo_control.id = tmp.tipo_control;
                            planilla.barrio.id = tmp.barrio;
                            planilla.encargado.id = tmp.encargado;
                            planilla.operador.id = tmp.operador;
                            planilla.supervisor.id = tmp.supervisor;
                            
                            $scope.postPlanilla( planilla );
                            
                            /*RestService.crearPlanilla(planilla).then(
                            function (response) {

                                if( response.data['respuesta'] > 0){
                                    var datos = [response.data['respuesta'], true, planilla.id];
                                    var query = 'UPDATE S1 SET id_server = ?, enviado = ? WHERE id = ?';
                                    $cordovaSQLite.execute(db, query, datos).then(
                                    function(res) {
                                        alert('Update id_server barrido -> ' + JSON.stringify(res));
                                    }, function(err) {
                                        alert('Error Update S1: ' + JSON.stringify(err));
                                    });
 
                                }else{
                                    alert(response.data['respuesta'])
                                }
                            }, function (err) {
                                alert( "error: " + JSON.stringify(err));
                            });*/  
                            
                        }

                    } else {
                        //alert('No hay planillas para barrido');
                    }
                }, function(err) {
                    alert('Error planilla S1: ' + JSON.stringify(err));
                });
                
            };
            
            $scope.postPlanilla = function( planilla ) {
                            return RestService.crearPlanilla(planilla).then(
                            function (response) {
                                //alert("insertar s1 data: " + JSON.stringify(response));
                                if( response.data['respuesta'] > 0){
                                    var datos = [response.data['respuesta'], true, planilla.id];
                                    var query = 'UPDATE S1 SET id_server = ?, enviado = ? WHERE id = ?';
                                    $cordovaSQLite.execute(db, query, datos).then(
                                    function(res) {
                                        //alert('Update id_server barrido -> ' + JSON.stringify(res));
                                    }, function(err) {
                                        alert('Error Update S1: ' + JSON.stringify(err));
                                    });
 
                                }else{
                                    //alert(response.data['respuesta'])
                                }
                            }, function (err) {
                                alert( "error: " + JSON.stringify(err));
                            });    
            };
            
            $scope.barrerRegistros = function() {
                /*var query = 'SELECT r.id, r.id_server, r.id_S1, s.id_server AS id_S1_server, r.manzana, r.direccion, r.latitude, r.longitude, r.verificado, r.predio, r.jefe_familia, r.habitantes, r.febriles, r.sin_criaderos, r.criaderos_eliminados, r.criaderos_tratados_fisicamente, r.criaderos_tratados_larvicida, r.larvicida_A, r.larvicida_B, r.larvicida_C, r.estado, r.fecha, r.enviado, cA.criaderos_A1, cA.criaderos_A2, cA.criaderos_A3, cA.criaderos_A4, cA.criaderos_A5, cA.criaderos_A6, cA.criaderos_A7, cA.criaderos_A8, cA.criaderos_A9, cA.criaderos_A10, cB.criaderos_B1, cB.criaderos_B2, cB.criaderos_B3, cB.criaderos_B4, cB.criaderos_B5, cB.criaderos_B6, cC.criaderos_C1, cC.criaderos_C2, cC.criaderos_C3, cC.criaderos_C4 FROM RegistrosS1 AS r JOIN S1 AS s ON r.id_S1 = s.id LEFT OUTER JOIN CriaderosA AS cA ON r.id = cA.id_registro_S1 LEFT OUTER JOIN CriaderosB AS cB ON r.id = cB.id_registro_S1 LEFT OUTER JOIN CriaderosC AS cC ON r.id = cC.id_registro_S1 WHERE r.estado = ? and r.enviado = ?';*/
                
                var query = 'SELECT r.id, r.id_server, r.id_S1, s.id_server AS id_S1_server, r.manzana, r.direccion, r.latitude, r.longitude, r.verificado, r.predio, r.jefe_familia, r.habitantes, r.febriles, r.sin_criaderos, r.criaderos_eliminados, r.criaderos_tratados_fisicamente, r.criaderos_tratados_larvicida, r.larvicida_A, r.estado, r.fecha, r.enviado, cA.criaderos_A1, cA.criaderos_A2, cA.criaderos_A3, cA.criaderos_A4, cA.criaderos_A5, cA.criaderos_A6, cA.criaderos_A7, cA.criaderos_A8, cA.criaderos_A9, cA.criaderos_A10, cB.criaderos_B1, cB.criaderos_B2, cB.criaderos_B3, cB.criaderos_B4, cB.criaderos_B5, cB.criaderos_B6, cC.criaderos_C1, cC.criaderos_C2, cC.criaderos_C3, cC.criaderos_C4 FROM RegistrosS1 AS r JOIN S1 AS s ON r.id_S1 = s.id LEFT OUTER JOIN CriaderosA AS cA ON r.id = cA.id_registro_S1 LEFT OUTER JOIN CriaderosB AS cB ON r.id = cB.id_registro_S1 LEFT OUTER JOIN CriaderosC AS cC ON r.id = cC.id_registro_S1 WHERE r.estado = ? and r.enviado = ?';

                $cordovaSQLite.execute(db, query, ["ACTIVO", false]).then(
                function(res) {
                    //alert('Lista registro S1: ' + JSON.stringify(res));
                    if (res.rows.length > 0) {
                        for (var i = 0; i < res.rows.length; i++) {
                            
                            //alert('item: ' + JSON.stringify(res.rows.item(i)));
                            var aux = res.rows.item(i);
                            var registro = {
                                id_S1: aux.id_S1,
                                id_S1_server: aux.id_S1_server,
                                manzana: aux.manzana,
                                direccion: aux.direccion,
                                latitude: aux.latitude,
                                longitude: aux.longitude,
                                verificado: aux.verificado,
                                predio: {id:null, nombre:null},
                                jefe_familia: aux.jefe_familia,
                                habitantes: aux.habitantes,
                                febriles: aux.febriles,
                                a1: aux.criaderos_A1,
                                a2: aux.criaderos_A2,
                                a3: aux.criaderos_A3,
                                a4: aux.criaderos_A4,
                                a5: aux.criaderos_A5,
                                a6: aux.criaderos_A6,
                                a7: aux.criaderos_A7,
                                a8: aux.criaderos_A8,
                                a9: aux.criaderos_A9,
                                a10: aux.criaderos_A10,
                                b1: aux.criaderos_B1,
                                b2: aux.criaderos_B2,
                                b3: aux.criaderos_B3,
                                b4: aux.criaderos_B4,
                                b5: aux.criaderos_B5,
                                b6: aux.criaderos_B6,
                                c1: aux.criaderos_C1,
                                c2: aux.criaderos_C2,
                                c3: aux.criaderos_C3,
                                c4: aux.criaderos_C4,
                                sin_criaderos: aux.sin_criaderos,
                                criaderos_eliminados: aux.criaderos_eliminados,
                                criaderos_tratados_fisicamente: aux.criaderos_tratados_fisicamente,
                                criaderos_tratados_larvicida: aux.criaderos_tratados_larvicida,
                                larvicida_A: aux.larvicida_A,
                                //larvicida_B: aux.larvicida_B,
                                //larvicida_C: aux.larvicida_C
                            };
                            registro.id = aux.id;
                            registro.predio.id = aux.predio;
                            
                            $scope.postRegistro( registro );

                            /*RestService.crearRegistro(registro).then(
                            function (response) {
                                //alert("insertar s1 data: " + JSON.stringify(response));
                                if( response.data['respuesta'] > 0){
                                    //alert("insertar s1 data: " + JSON.stringify(response));
                                    var datos = [response.data['respuesta'], true, registro.id];
                                    var query = 'UPDATE RegistrosS1 SET id_server = ?, enviado = ? WHERE id = ?';
                                    //alert("datos: " + datos);
                                    $cordovaSQLite.execute(db, query, datos).then(
                                    function(res) {
                                        //alert('Update id_server -> ' + res.insertId);
                                    }, function(err) {
                                        alert('Error Update RegistrosS1: ' + JSON.stringify(err));
                                    });
 
                                }else{
                                    //alert(response.data['respuesta']);    
                                }

                            }, function (err) {
                                alert( "error: " + JSON.stringify(err));
                            });*/   
                                
                        }

                    } else {
                        //alert('No hay registros para barrido');
                    }
                }, function(err) {
                    alert('Error registro S1: ' + JSON.stringify(err));
                });
            };
            
            $scope.postRegistro = function( registro ) {
                            return RestService.crearRegistro(registro).then(
                            function (response) {
                                //alert("insertar s1 data: " + JSON.stringify(response));
                                if( response.data['respuesta'] > 0){
                                    //alert("insertar s1 data: " + JSON.stringify(response));
                                    var datos = [response.data['respuesta'], true, registro.id];
                                    var query = 'UPDATE RegistrosS1 SET id_server = ?, enviado = ? WHERE id = ?';
                                    //alert("datos: " + datos);
                                    $cordovaSQLite.execute(db, query, datos).then(
                                    function(res) {
                                        //alert('Update id_server -> ' + res.insertId);
                                    }, function(err) {
                                        alert('Error Update RegistrosS1: ' + JSON.stringify(err));
                                    });
 
                                }else{
                                    //alert(response.data['respuesta']);    
                                }

                            }, function (err) {
                                alert( "error: " + JSON.stringify(err));
                            });   
            };
            
            $scope.select = function(estado) {                
                $scope.events = [];
                var fecha = $filter('date')(new Date(), 'dd/MM/yyyy');
                
                var query = 'SELECT s.id, tp.nombre AS tipo_control, dep.nombre AS departamento, dis.nombre AS distrito, bar.nombre AS barrio, usu.nombre AS usuario, s.fecha AS fecha FROM S1 AS s JOIN TipoControl AS tp ON s.tipo_control = tp.id JOIN Departamentos AS dep ON s.departamento = dep.id JOIN Distritos AS dis ON s.distrito = dis.id JOIN Barrios AS bar ON s.barrio = bar.id JOIN Usuarios AS usu ON s.usuario = usu.id WHERE s.fecha = ? and s.estado = ? and s.usuario = ?';

                $cordovaSQLite.execute(db, query, [ fecha, estado, $rootScope.loginId]).then(
                function(res) {

                    if (res.rows.length > 0) {
                        for (var i = 0; i < res.rows.length; i++) {
                            $scope.events.push(res.rows.item(i));
                        }

                    } else {
                        //alert('No results found');
                    }
                }, function(err) {
                    //alert('Error Lista S1: ' + JSON.stringify(err));
                });
                
            };
            setTimeout(function() {$scope.select('ACTIVO')}, 500);

            $scope.selectPlanilla = function(id) {
                
              var query = 'SELECT s.id, s.tipo_control, s.departamento, s.distrito, s.barrio, s.encargado, s.operador, s.supervisor, s.usuario, s.fecha, s.estado FROM S1 AS s WHERE s.estado = "ACTIVO" and s.id = ? and s.usuario = ?';
               
              $cordovaSQLite.execute(db, query, [id, $rootScope.loginId]).then(function(res) {
                  if(res.rows.length > 0) {
                      $scope.planilla.id = res.rows.item(0).id;
                      $scope.planilla.tipo_control = res.rows.item(0).tipo_control;
                      $scope.planilla.departamento = res.rows.item(0).departamento;
                      $scope.planilla.distrito = res.rows.item(0).distrito;
                      $scope.planilla.barrio = res.rows.item(0).barrio;
                      $scope.planilla.encargado = res.rows.item(0).encargado;
                      $scope.planilla.operador = res.rows.item(0).operador;
                      $scope.planilla.supervisor = res.rows.item(0).supervisor;
                      $scope.planilla.cuadrilla = res.rows.item(0).cuadrilla;
                      $scope.planilla.brigada = res.rows.item(0).brigada;
                      $scope.planilla.usuario = res.rows.item(0).usuario; 
                      $scope.planilla.fecha = res.rows.item(0).fecha; 
                      $scope.planilla.estado = res.rows.item(0).estado;
                      
                      alert("resultado select s1: " + JSON.stringify($scope.planilla));
                      
                  } else {
                      //alert('No results found');
                  }
              }, function (err) {
                  alert('Error Lista S1: ' + err);
              });             
                
            };

            $scope.modificarPlanilla = function(id) {
                $location.path('/nueva-planilla/' + id);
            };


        }, false);

        // An alert dialog
        $scope.showAlert = function() {
            var alertPopup = $ionicPopup.alert({
                title: 'Info',
                template: 'La operación se realizó con éxito'
            });

            alertPopup.then(function(res) {
                //$location.path('/login');
            });
        };
        $scope.agregarS1 = function() {
            $location.path('/nueva-planilla/' + 0);
        };
        $scope.irPantallaRegistros = function(id) {
            $location.path('/registros/' + id);
        };

        $scope.hideBackButton = false;
        $scope.registro = {
            id: null,
            id_server: null,
            id_S1: null,
            id_S1_server: null,
            id_cA: null,
            id_cB: null,
            id_cC: null,
            manzana: '',
            direccion: '',
            latitude: null,
            longitude: null,
            precision: null,
            altitud: null,
            verificado: false,
            predio: null,
            jefe_familia: '',
            habitantes: null,
            febriles: null,
            a1: null,
            a2: null,
            a3: null,
            a4: null,
            a5: null,
            a6: null,
            a7: null,
            a8: null,
            a9: null,
            a10: null,
            b1: null,
            b2: null,
            b3: null,
            b4: null,
            b5: null,
            b6: null,
            c1: null,
            c2: null,
            c3: null,
            c4: null,
            sin_criaderos: false,
            criaderos_eliminados: null,
            criaderos_tratados_fisicamente: null,
            criaderos_tratados_larvicida: null,
            larvicida_A: null,
            //larvicida_B: null,
            //larvicida_C: null,
            fecha: '',
            estado: '',
            enviado: false
        };

        $scope.planilla = {
            id: null,
            id_server: null,
            tipo_control: null,
            departamento: null,
            distrito: null,
            barrio: null,
            encargado: null,
            operador: null,
            supervisor: null,
            cuadrilla: '',
            brigada: '',
            usuario: null,
            fecha: '',
            estado: ''
        }

        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            if (/*(toState.name == 'done') ||*/ (toState.name == 'step1') /*|| (toState.name == 'registros')*/ )
                $scope.hideBackButton = true;
            else
                $scope.hideBackButton = false;
        });
    })

.controller('NuevaPlanillaCtrl', function($scope, $location, $ionicPopup, $cordovaSQLite, $filter, $stateParams, RestService, $rootScope, $ionicPlatform, $state) {

    $scope.sw1 = true;
    $scope.sw2 = true;

    document.addEventListener('deviceready', function() {

        var db = window.sqlitePlugin.openDatabase({
            name: 'my.db',
            location: 'default'
        });

        $scope.selectPlanilla = function(id) {
 
            if ($stateParams.id != '0') {
                
                var query = 'SELECT s.id, s.id_server, s.tipo_control, s.departamento, s.distrito, s.barrio, s.encargado, s.operador, s.supervisor, s.usuario, s.fecha, s.estado, s.cuadrilla, s.brigada FROM S1 AS s WHERE s.estado = "ACTIVO" and s.id = ? and s.usuario = ?';
                $cordovaSQLite.execute(db, query, [id, $rootScope.loginId]).then(function(res) {
                    if (res.rows.length > 0) {
                        //alert("planilla tipo control:" + $scope.planilla.tipo_control);
                        for (var i = 0; i < res.rows.length; i++) {
                            $scope.planilla.id = res.rows.item(0).id;
                            $scope.planilla.id_server = res.rows.item(0).id_server;
                            $scope.planilla.tipo_control = res.rows.item(0).tipo_control;
                            $scope.planilla.departamento = res.rows.item(0).departamento;
                            $scope.planilla.distrito = res.rows.item(0).distrito;
                            $scope.planilla.barrio = res.rows.item(0).barrio;
                            $scope.planilla.encargado = res.rows.item(0).encargado;
                            $scope.planilla.operador = res.rows.item(0).operador;
                            $scope.planilla.supervisor = res.rows.item(0).supervisor;
                            $scope.planilla.cuadrilla = res.rows.item(0).cuadrilla;
                            $scope.planilla.brigada = res.rows.item(0).brigada;
                            $scope.planilla.usuario = res.rows.item(0).usuario;
                            $scope.planilla.fecha = res.rows.item(0).fecha;
                            $scope.planilla.estado = res.rows.item(0).estado;

                        }

                    } else {
                        //alert('No results found');
                    }
                }, function(err) {
                    alert('Error Lista Planilla: ' + err);
                });
                
            }else{
                $scope.planilla.id = null;
                $scope.planilla.id_server = null;
                $scope.planilla.tipo_control = null;
                $scope.planilla.departamento = null;
                $scope.planilla.distrito = null;
                $scope.planilla.barrio = null;
                $scope.planilla.encargado = null;
                $scope.planilla.operador = null;
                $scope.planilla.supervisor = null;
                $scope.planilla.cuadrilla = '';
                $scope.planilla.brigada = '';
                $scope.planilla.usuario = null;
                $scope.planilla.fecha = '';
                $scope.planilla.estado = '';    
            }
        };
        $scope.selectPlanilla($stateParams.id);


        $scope.selectListaTipoControl = function() {
            $scope.list_tipo_control = [];

            var query = 'SELECT id, nombre FROM TipoControl';
            $cordovaSQLite.execute(db, query, []).then(function(res) {
                if (res.rows.length > 0) {
                    for (var i = 0; i < res.rows.length; i++) {
                        $scope.list_tipo_control.push(res.rows.item(i));
                        if ($scope.list_tipo_control[i].id == $scope.planilla.tipo_control) {
                            $scope.planilla.tipo_control = $scope.list_tipo_control[i];
                        }
                    }

                } else {
                    //alert('No se encontraron Tipos de Control');
                }
            }, function(err) {
                alert('Error Lista TipoControl: ' + err);
            });
            
        };
        $scope.selectListaTipoControl();


        $scope.selectListaDepartamento = function() {
            $scope.list_departamento = [];
            var query = 'SELECT id, nombre FROM Departamentos';
            $cordovaSQLite.execute(db, query, []).then(function(res) {
                if (res.rows.length > 0) {
                    for (var i = 0; i < res.rows.length; i++) {
                        $scope.list_departamento.push(res.rows.item(i));
                        if ($scope.list_departamento[i].id == $scope.planilla.departamento) {
                            $scope.planilla.departamento = $scope.list_departamento[i];
                        }
                    }

                } else {
                    //alert('No se encontraron Departamentos');
                }
            }, function(err) {
                alert('Error Lista Departamentos: ' + err);
            });
            
        };
        $scope.selectListaDepartamento();


        $scope.selectListaDistritoPorDepartamento = function() {
            
                $scope.list_distrito = [];
                var query = 'SELECT id, nombre FROM Distritos WHERE id_departamento = ?';
                $cordovaSQLite.execute(db, query, [$scope.planilla.departamento.id]).then(function(res) {
                    if (res.rows.length > 0) {
                        for (var i = 0; i < res.rows.length; i++) {
                            $scope.list_distrito.push(res.rows.item(i));
                            if ($scope.list_distrito[i].id == $scope.planilla.distrito) {
                                $scope.planilla.distrito = $scope.list_distrito[i];
                            }
                        }
                        //alert("resultado: " + JSON.stringify($scope.list_tipo_control));
                    } else {
                        //alert('No se encontraron Distritos');    
                    }
                }, function(err) {
                    alert('Error Lista Distritos: ' + err);
                });

            $scope.sw1 = false;
        };
        //$scope.selectListaDistritoPorDepartamento();
        
        $scope.selectListaDistrito = function() {

                $scope.list_distrito = [];
                var query = 'SELECT id, nombre FROM Distritos';
                $cordovaSQLite.execute(db, query, []).then(function(res) {
                    if (res.rows.length > 0) {
                        for (var i = 0; i < res.rows.length; i++) {
                            $scope.list_distrito.push(res.rows.item(i));
                            if ($scope.list_distrito[i].id == $scope.planilla.distrito) {
                                $scope.planilla.distrito = $scope.list_distrito[i];
                            }
                        }
                        //alert("resultado: " + JSON.stringify($scope.list_tipo_control));

                    } else {
                        //alert('No se encontraron Distritos');
                    }
                }, function(err) {
                    alert('Error Lista Distritos: ' + err);
                });
            
            $scope.sw1 = false;
        };
        $scope.selectListaDistrito();


        $scope.selectListaBarrioPorDistrito = function() {
            $scope.list_barrio = [];
            var query = 'SELECT id, nombre FROM Barrios WHERE id_distrito = ?';
            $cordovaSQLite.execute(db, query, [$scope.planilla.distrito.id]).then(function(res) {
                if (res.rows.length > 0) {
                    for (var i = 0; i < res.rows.length; i++) {
                        $scope.list_barrio.push(res.rows.item(i));
                        if ($scope.list_barrio[i].id == $scope.planilla.barrio) {
                            $scope.planilla.barrio = $scope.list_barrio[i];
                        }
                    }
                    //alert("resultado: " + JSON.stringify($scope.list_tipo_control));

                } else {
                    //alert('No se encontraron Barrios');
                }
            }, function(err) {
                alert('Error Lista Barrios: ' + err);
            });
            
            $scope.sw2 = false;
        };
        //$scope.selectListaBarrioPorDistrito();
        
        $scope.selectListaBarrio = function() {
            $scope.list_barrio = [];
            var query = 'SELECT id, nombre FROM Barrios';
            $cordovaSQLite.execute(db, query, []).then(function(res) {
                if (res.rows.length > 0) {
                    for (var i = 0; i < res.rows.length; i++) {
                        $scope.list_barrio.push(res.rows.item(i));
                        if ($scope.list_barrio[i].id == $scope.planilla.barrio) {
                            $scope.planilla.barrio = $scope.list_barrio[i];
                        }
                    }
                    //alert("resultado: " + JSON.stringify($scope.list_tipo_control));

                } else {
                    //alert('No se encontraron Barrios');
                }
            }, function(err) {
                alert('Error Lista Barrios: ' + err);
            });
            
            $scope.sw2 = false;
        };
        $scope.selectListaBarrio();


        $scope.selectListaUsuario = function() {
            $scope.list_usuario = [];
            var query = 'SELECT id, nombre, apellido, cedula FROM Usuarios WHERE id != 1';
            $cordovaSQLite.execute(db, query, []).then(function(res) {
                if (res.rows.length > 0) {
                    for (var i = 0; i < res.rows.length; i++) {
                        $scope.list_usuario.push(res.rows.item(i));
                        if ($scope.list_usuario[i].id == $scope.planilla.encargado) {
                            $scope.planilla.encargado = $scope.list_usuario[i];
                        }
                        if ($scope.list_usuario[i].id == $scope.planilla.operador) {
                            $scope.planilla.operador = $scope.list_usuario[i];
                        }
                        if ($scope.list_usuario[i].id == $scope.planilla.supervisor) {
                            $scope.planilla.supervisor = $scope.list_usuario[i];
                        }
                        if ($scope.list_usuario[i].id == $scope.planilla.usuario) {
                            $scope.planilla.usuario = $scope.list_usuario[i];
                        }
                    }

                } else {
                    //alert('No se encontraron Usuarios');
                }
            }, function(err) {
                alert('Error Lista Usuarios: ' + err);
            });
            
        };
        $scope.selectListaUsuario();

        $scope.insertarS1 = function() {
            
            $scope.planilla.usuario = $rootScope.loginId;
            $scope.planilla.fecha = $filter('date')(new Date(), 'dd/MM/yyyy');
            $scope.planilla.estado = 'ACTIVO';

            var datos = [$scope.planilla.id_server, $scope.planilla.tipo_control.id, $scope.planilla.departamento.id, $scope.planilla.distrito.id, $scope.planilla.barrio.id, $scope.planilla.encargado.id, $scope.planilla.supervisor.id, $scope.planilla.operador.id, $scope.planilla.cuadrilla, $scope.planilla.brigada, $scope.planilla.usuario, $scope.planilla.fecha, $scope.planilla.estado, false];

            var query = 'INSERT INTO S1 (id_server, tipo_control, departamento, distrito, barrio, encargado, supervisor, operador, cuadrilla, brigada, usuario, fecha, estado, enviado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
            $cordovaSQLite.execute(db, query, datos).then(
            function(res) {
                
                RestService.crearPlanilla($scope.planilla).then(
                function (response) {

                    if( response.data['respuesta'] > 0){
                        $scope.planilla.id = res.insertId;
                        var datos = [response.data['respuesta'], true, $scope.planilla.id];
                        var query = 'UPDATE S1 SET id_server = ?, enviado = ? WHERE id = ?';
                        $cordovaSQLite.execute(db, query, datos).then(
                        function(res) {
                            //alert('Update id_server -> ' + res.insertId);
                        }, function(err) {
                            alert('Error Update S1: ' + JSON.stringify(err));
                        });
  
                    }else{
                        alert(response.data['respuesta']);    
                    }             
                }, function (err) {
                    alert( "error: " + err);
                });

                $scope.planilla.id = null;
                $scope.planilla.id_server = null;
                $scope.planilla.tipo_control = null;
                $scope.planilla.departamento = null;
                $scope.planilla.distrito = null;
                $scope.planilla.barrio = null;
                $scope.planilla.encargado = null;
                $scope.planilla.operador = null;
                $scope.planilla.supervisor = null;
                $scope.planilla.cuadrilla = '';
                $scope.planilla.brigada = '';
                $scope.planilla.usuario = null;
                $scope.planilla.fecha = '';
                $scope.planilla.estado = '';
                
            }, function(err) {
                alert('Error Insertar S1: ' + err);
            });

        };

        $scope.modificarS1 = function() {

            $scope.planilla.usuario = $rootScope.loginId;
            $scope.planilla.fecha = $filter('date')(new Date(), 'dd/MM/yyyy');
            $scope.planilla.estado = 'ACTIVO';

            var datos = [$scope.planilla.tipo_control.id, $scope.planilla.departamento.id, $scope.planilla.distrito.id, $scope.planilla.barrio.id, $scope.planilla.encargado.id, $scope.planilla.supervisor.id, $scope.planilla.operador.id, $scope.planilla.cuadrilla, $scope.planilla.brigada, $scope.planilla.usuario, $scope.planilla.fecha, false, $scope.planilla.id];

            var query = 'UPDATE S1 SET tipo_control = ?, departamento = ?, distrito = ?, barrio = ?, encargado = ?, supervisor = ?, operador = ?, cuadrilla = ?, brigada = ?, usuario = ?, fecha = ?, enviado = ? WHERE id = ?';
            $cordovaSQLite.execute(db, query, datos).then(
            function(res) {
         
                RestService.modificarPlanilla($scope.planilla).then(
                function (response) {

                    if( response.data['respuesta'] == "OK"){

                        var datos = [true, $scope.planilla.id];
                        var query = 'UPDATE S1 SET enviado = ? WHERE id = ?';
                        $cordovaSQLite.execute(db, query, datos).then(
                        function(res) {
                            //alert('Update id_server -> ' + res.insertId);
                        }, function(err) {
                            alert('Error Update S1: ' + JSON.stringify(err));
                        });
 
                    }

                }, function (err) {
                    alert( "error: " + JSON.stringify(err));
                });
                
                $scope.planilla.id = null;
                $scope.planilla.id_server = null;
                $scope.planilla.tipo_control = null;
                $scope.planilla.departamento = null;
                $scope.planilla.distrito = null;
                $scope.planilla.barrio = null;
                $scope.planilla.encargado = null;
                $scope.planilla.operador = null;
                $scope.planilla.supervisor = null;
                $scope.planilla.cuadrilla = '';
                $scope.planilla.brigada = '';
                $scope.planilla.usuario = null;
                $scope.planilla.fecha = '';
                $scope.planilla.estado = '';
                
            }, function(err) {
                alert('Error modificar S1: ' + JSON.stringify(err));
            });
            
        };

        $scope.guardar = function() {

            $scope.validarDatos();
            if( $scope.sw === true ){
                if ($scope.planilla.id === null) {

                    var fecha = $filter('date')(new Date(), 'dd/MM/yyyy');
                    var datos = [$scope.planilla.tipo_control.id, $scope.planilla.departamento.id, $scope.planilla.distrito.id, $scope.planilla.barrio.id, $scope.planilla.cuadrilla, $scope.planilla.brigada, fecha, "ACTIVO", $rootScope.loginId];

                    var query = 'SELECT * FROM S1 WHERE tipo_control = ? and departamento = ? and distrito = ? and barrio = ? and cuadrilla = ? and brigada= ? and fecha = ? and estado = ? and usuario = ?';

                    $cordovaSQLite.execute(db, query, datos).then(
                    function(res) {
                        if(res.rows.length <= 0){
                            $scope.insertarS1();
                            $location.path('/tab/operaciones');
                            $scope.showAlert();    
                        }else{
                            alert("La planilla ya existe");    
                        }

                    }, function(err) {
                        alert('Error verificar Insertar S1: ' + err);
                    });

                } else {
                    $scope.modificarS1();
                    $location.path('/tab/operaciones');
                    $scope.showAlert();
                }
            }

        };
        
        $scope.validarDatos = function() {
            $scope.sw = true;  
            if($scope.planilla.tipo_control === null){
                alert("Ingrese el Tipo de Control");
                $scope.sw = false;

            }else if($scope.planilla.departamento === null){
                alert("Ingrese el Departamento");
                $scope.sw = false;
                
            }else if($scope.planilla.distrito === null){
                alert("Ingrese el Distrito");
                $scope.sw = false;
                
            }else if($scope.planilla.barrio === null){
                alert("Ingrese el Barrio");
                $scope.sw = false;

            }else if($scope.planilla.encargado === null){
                alert("Ingrese el Encargado");
                $scope.sw = false;
                
            }else if($scope.planilla.operador === null){
                alert("Ingrese el Operador");
                $scope.sw = false;
                
            }else if($scope.planilla.supervisor === null){
                alert("Ingrese el Supervisor");
                $scope.sw = false;
                
            }else if($scope.planilla.cuadrilla === '' || $scope.planilla.cuadrilla === null){
                alert("Ingrese el Cuadrilla");
                $scope.sw = false;
                
            }else if($scope.planilla.brigada === '' || $scope.planilla.brigada === null){
                alert("Ingrese la Brigada");
                $scope.sw = false;
                
            }
        }

    }, false);

    $scope.flgGuardar = false;
    // An alert dialog
    $scope.showAlert = function() {
        var alertPopup = $ionicPopup.alert({
            title: 'Info',
            template: 'La operación se realizó con éxito'
        });

        alertPopup.then(function(res) {
            //$location.path('/login');
        });
    };
    $scope.cancelar = function() {
        $scope.flgGuardar = false;
        $scope.planilla.id = null,
            $scope.planilla.tipo_control = null,
            $scope.planilla.departamento = null,
            $scope.planilla.distrito = null,
            $scope.planilla.barrio = null,
            $scope.planilla.encargado = null,
            $scope.planilla.operador = null,
            $scope.planilla.supervisor = null,
            $scope.planilla.cuadrilla = '',
            $scope.planilla.brigada = '',
            $scope.planilla.usuario = null,
            $scope.planilla.fecha = '',
            $scope.planilla.estado = ''
        $location.path('/tab/operaciones');
    };

    $scope.showConfirm = function() {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Cancelar',
            template: 'Desea cancelar la operación?'
        });

        confirmPopup.then(function(res) {
            if (res) {
                $scope.cancelar();
            }
        });
    };
    
    $ionicPlatform.registerBackButtonAction(function() {
        //var path = $location.path()
        //alert("state: " + $state.current.name);
        if ($state.current.name === "nueva-planilla") {
            //navigator.app.exitApp();
            $scope.showConfirm();       
        }
    }, 100);

})

.controller('RegistrosCtrl', function($scope, $location, $ionicPopup, $cordovaSQLite, $stateParams, $ionicHistory, $ionicPlatform, $state) {

    document.addEventListener('deviceready', function() {

        var db = window.sqlitePlugin.openDatabase({
            name: 'my.db',
            location: 'default'
        });
        
        $scope.registro.id_S1 = parseInt($stateParams.id);
        
        $scope.ObtenerId_server = function() {
            var query = 'SELECT id_server FROM S1 WHERE id = ?';
            $cordovaSQLite.execute(db, query, [$scope.registro.id_S1]).then(function(res) {
                $scope.registro.id_S1_server = res.rows.item(0).id_server;

            }, function(err) {
                alert('Error: ' + JSON.stringify(err));
            });    
        }
        $scope.ObtenerId_server();

        $scope.select = function(id) {

            /*var query = 'SELECT r.id, r.manzana, r.direccion, r.latitude, r.longitude, r.verificado, tp.nombre AS predio, r.jefe_familia, r.habitantes, r.febriles, r.sin_criaderos, r.criaderos_eliminados, r.criaderos_tratados_fisicamente, r.criaderos_tratados_larvicida, r.larvicida_A, r.larvicida_B, r.larvicida_C, r.fecha, r.estado, r.enviado FROM RegistrosS1 AS r JOIN S1 AS s ON r.id_S1 = s.id JOIN TiposPredio AS tp ON r.predio = tp.id WHERE r.estado = "ACTIVO" and s.id = ?';*/
            
            var query = 'SELECT r.id, r.manzana, r.direccion, r.latitude, r.longitude, r.verificado, tp.nombre AS predio, r.jefe_familia, r.habitantes, r.febriles, r.sin_criaderos, r.criaderos_eliminados, r.criaderos_tratados_fisicamente, r.criaderos_tratados_larvicida, r.larvicida_A, r.fecha, r.estado, r.enviado FROM RegistrosS1 AS r JOIN S1 AS s ON r.id_S1 = s.id JOIN TiposPredio AS tp ON r.predio = tp.id WHERE r.estado = "ACTIVO" and s.id = ?';

            //alert(query);
            $scope.records = [];
            $cordovaSQLite.execute(db, query, [id]).then(function(res) {
                if (res.rows.length > 0) {
                    for (var i = 0; i < res.rows.length; i++) {
                        $scope.records.push(res.rows.item(i));
                    }

                } else {
                    //alert('No results found');
                }
            }, function(err) {
                alert('Error Lista RegistrosS1: ' + JSON.stringify(err));
            });
        };

        $scope.select($scope.registro.id_S1);


        // An alert dialog
        $scope.showAlert = function() {
            var alertPopup = $ionicPopup.alert({
                title: 'Info',
                template: 'La operación se realizó con éxito'
            });

            alertPopup.then(function(res) {
                //$location.path('/login');
            });
        };
        $scope.agregarRegistro = function() {
            $location.path('/step1');
        };
        $scope.cancelar = function() {
            $location.path('/tab/operaciones');
        };

        $scope.selectRegistro = function(id) {

            /*var query = 'SELECT r.id, r.id_server, r.id_S1, r.manzana, r.direccion, r.latitude, r.longitude, r.verificado, tp.id AS predio, r.jefe_familia, r.habitantes, r.febriles, r.sin_criaderos, r.criaderos_eliminados, r.criaderos_tratados_fisicamente, r.criaderos_tratados_larvicida, r.larvicida_A, r.larvicida_B, r.larvicida_C, r.fecha, r.estado, r.enviado, cA.id As id_criaderos_A, cA.criaderos_A1, cA.criaderos_A2, cA.criaderos_A3, cA.criaderos_A4, cA.criaderos_A5, cA.criaderos_A6, cA.criaderos_A7, cA.criaderos_A8, cA.criaderos_A9, cA.criaderos_A10, cB.id As id_criaderos_B, cB.criaderos_B1, cB.criaderos_B2, cB.criaderos_B3, cB.criaderos_B4, cB.criaderos_B5, cB.criaderos_B6, cC.id As id_criaderos_C, cC.criaderos_C1, cC.criaderos_C2, cC.criaderos_C3, cC.criaderos_C4 FROM RegistrosS1 AS r JOIN S1 AS s ON r.id_S1 = s.id JOIN TiposPredio AS tp ON r.predio = tp.id  LEFT OUTER JOIN CriaderosA AS cA ON r.id = cA.id_registro_S1 LEFT OUTER JOIN CriaderosB AS cB ON r.id = cB.id_registro_S1 LEFT OUTER JOIN CriaderosC AS cC ON r.id = cC.id_registro_S1 WHERE r.estado = "ACTIVO" and r.id = ?';*/
            
            var query = 'SELECT r.id, r.id_server, r.id_S1, r.manzana, r.direccion, r.latitude, r.longitude, r.verificado, tp.id AS predio, r.jefe_familia, r.habitantes, r.febriles, r.sin_criaderos, r.criaderos_eliminados, r.criaderos_tratados_fisicamente, r.criaderos_tratados_larvicida, r.larvicida_A, r.fecha, r.estado, r.enviado, cA.id As id_criaderos_A, cA.criaderos_A1, cA.criaderos_A2, cA.criaderos_A3, cA.criaderos_A4, cA.criaderos_A5, cA.criaderos_A6, cA.criaderos_A7, cA.criaderos_A8, cA.criaderos_A9, cA.criaderos_A10, cB.id As id_criaderos_B, cB.criaderos_B1, cB.criaderos_B2, cB.criaderos_B3, cB.criaderos_B4, cB.criaderos_B5, cB.criaderos_B6, cC.id As id_criaderos_C, cC.criaderos_C1, cC.criaderos_C2, cC.criaderos_C3, cC.criaderos_C4 FROM RegistrosS1 AS r JOIN S1 AS s ON r.id_S1 = s.id JOIN TiposPredio AS tp ON r.predio = tp.id  LEFT OUTER JOIN CriaderosA AS cA ON r.id = cA.id_registro_S1 LEFT OUTER JOIN CriaderosB AS cB ON r.id = cB.id_registro_S1 LEFT OUTER JOIN CriaderosC AS cC ON r.id = cC.id_registro_S1 WHERE r.estado = "ACTIVO" and r.id = ?';

            $cordovaSQLite.execute(db, query, [id]).then(function(res) {
                if (res.rows.length > 0) {
                    $scope.registro.id = res.rows.item(0).id;
                    $scope.registro.id_server = res.rows.item(0).id_server;
                    $scope.registro.id_S1 = res.rows.item(0).id_S1;
                    $scope.registro.id_cA = res.rows.item(0).id_criaderos_A;
                    $scope.registro.id_cB = res.rows.item(0).id_criaderos_B;
                    $scope.registro.id_cC = res.rows.item(0).id_criaderos_C;
                    $scope.registro.manzana = res.rows.item(0).manzana;
                    $scope.registro.direccion = res.rows.item(0).direccion;
                    //$scope.registro.coordenada = res.rows.item(0).coordenada;
                    $scope.registro.verificado = res.rows.item(0).verificado;
                    $scope.registro.predio = res.rows.item(0).predio;
                    $scope.registro.jefe_familia = res.rows.item(0).jefe_familia;
                    $scope.registro.habitantes = res.rows.item(0).habitantes;
                    $scope.registro.febriles = res.rows.item(0).febriles;
                    $scope.registro.sin_criaderos = res.rows.item(0).sin_criaderos;
                    $scope.registro.criaderos_eliminados = res.rows.item(0).criaderos_eliminados;
                    $scope.registro.criaderos_tratados_fisicamente = res.rows.item(0).criaderos_tratados_fisicamente;
                    $scope.registro.criaderos_tratados_larvicida = res.rows.item(0).criaderos_tratados_larvicida;
                    $scope.registro.larvicida_A = res.rows.item(0).larvicida_A;
                    //$scope.registro.larvicida_B = res.rows.item(0).larvicida_B;
                    //$scope.registro.larvicida_C = res.rows.item(0).larvicida_C;
                    $scope.registro.fecha = res.rows.item(0).fecha;
                    $scope.registro.estado = res.rows.item(0).estado;
                    $scope.registro.a1 = res.rows.item(0).criaderos_A1;
                    $scope.registro.a2 = res.rows.item(0).criaderos_A2;
                    $scope.registro.a3 = res.rows.item(0).criaderos_A3;
                    $scope.registro.a4 = res.rows.item(0).criaderos_A4;
                    $scope.registro.a5 = res.rows.item(0).criaderos_A5;
                    $scope.registro.a6 = res.rows.item(0).criaderos_A6;
                    $scope.registro.a7 = res.rows.item(0).criaderos_A7;
                    $scope.registro.a8 = res.rows.item(0).criaderos_A8;
                    $scope.registro.a9 = res.rows.item(0).criaderos_A9;
                    $scope.registro.a10 = res.rows.item(0).criaderos_A10;
                    $scope.registro.b1 = res.rows.item(0).criaderos_B1;
                    $scope.registro.b2 = res.rows.item(0).criaderos_B2;
                    $scope.registro.b3 = res.rows.item(0).criaderos_B3;
                    $scope.registro.b4 = res.rows.item(0).criaderos_B4;
                    $scope.registro.b5 = res.rows.item(0).criaderos_B5;
                    $scope.registro.b6 = res.rows.item(0).criaderos_B6;
                    $scope.registro.c1 = res.rows.item(0).criaderos_C1;
                    $scope.registro.c2 = res.rows.item(0).criaderos_C2;
                    $scope.registro.c3 = res.rows.item(0).criaderos_C3;
                    $scope.registro.c4 = res.rows.item(0).criaderos_C4;
                    $scope.registro.enviado = res.rows.item(0).enviado;

                    if (res.rows.item(0).verificado === 'true') {
                        $scope.registro.verificado = true;
                    } else {
                        $scope.registro.verificado = false;
                    }

                    if (res.rows.item(0).sin_criaderos === 'true') {
                        $scope.registro.sin_criaderos = true;
                    } else {
                        $scope.registro.sin_criaderos = false;
                    }

                } else {
                    //alert('No results found');
                }
            }, function(err) {
                alert('Error Lista RegistrosS1: ' + err);
            });

        };

        $scope.modificarRegistro = function(id) {
            $scope.selectRegistro(id);
            $location.path('/step1');
        };

    }, false);
    
    $ionicPlatform.registerBackButtonAction(function() {
        //var path = $location.path()
        //alert("state: " + $state.current.name);
        if ($state.current.name === "registros") {
            //$scope.showConfirm();
            //$scope.cancelar();
            //$ionicHistory.goBack();
            $state.go('tab.operaciones');
        }
    }, 100);

})

.controller('Step1Ctrl', function($scope, $rootScope, $state,
    $location, $ionicPopup, $cordovaSQLite,
    $filter, $cordovaGeolocation, $ionicLoading, $http, RestService, $ionicPlatform) {
    $scope.step1Submitted = false;

    $scope.submit = function() {
        $scope.step1Submitted = true;
    };

    // An alert dialog
    $scope.showAlert = function() {
        var alertPopup = $ionicPopup.alert({
            title: 'Info',
            template: 'La operación se realizó con éxito'
        });
        alertPopup.then(function(res) {
            if (res) {
                //setTimeout(function() {$location.path('/registros/' + $scope.registro.id_S1)}, 500);
                $location.path('/registros/' + $scope.registro.id_S1);
                $scope.inicializarRegistro();
            }
        });
    };

    $scope.showConfirm = function() {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Cancelar',
            template: 'Desea cancelar la operación?'
        });

        confirmPopup.then(function(res) {
            if (res) {
                $scope.cancelar();
            }
        });
    };

    $scope.cancelar = function() {
        
        $scope.registro.id = null;
        $scope.registro.id_cA = null;
        $scope.registro.id_cB = null;
        $scope.registro.id_cC = null;
        $scope.registro.manzana = '';
        $scope.registro.direccion = '';
        //$scope.registro.coordenada =  '';
        $scope.registro.latitude = null;
        $scope.registro.longitude = null;
        $scope.registro.precision = null;
        $scope.registro.altitud = null;
        $scope.registro.verificado = false;
        $scope.registro.predio = null;
        $scope.registro.jefe_familia = '';
        $scope.registro.habitantes = null;
        $scope.registro.febriles = null;
        $scope.registro.a1 = null;
        $scope.registro.a2 = null;
        $scope.registro.a3 = null;
        $scope.registro.a4 = null;
        $scope.registro.a5 = null;
        $scope.registro.a6 = null;
        $scope.registro.a7 = null;
        $scope.registro.a8 = null;
        $scope.registro.a9 = null;
        $scope.registro.a10 = null;
        $scope.registro.b1 = null;
        $scope.registro.b2 = null;
        $scope.registro.b3 = null;
        $scope.registro.b4 = null;
        $scope.registro.b5 = null;
        $scope.registro.b6 = null;
        $scope.registro.c1 = null;
        $scope.registro.c2 = null;
        $scope.registro.c3 = null;
        $scope.registro.c4 = null;
        $scope.registro.sin_criaderos = false;
        $scope.registro.criaderos_eliminados = null;
        $scope.registro.criaderos_tratados_fisicamente = null;
        $scope.registro.criaderos_tratados_larvicida = null;
        $scope.registro.larvicida_A = null;
        //$scope.registro.larvicida_B = null;
        //$scope.registro.larvicida_C = null;
        $scope.registro.fecha = '';
        $scope.registro.estado = '';
        $location.path('/registros/' + $scope.registro.id_S1);
    };


    document.addEventListener('deviceready', function() {

        var db = window.sqlitePlugin.openDatabase({
            name: 'my.db',
            location: 'default'
        });

        $scope.obtenerCoordenadas = function() {
            var posOptions = {
                timeout: 10000,
                maximumAge: 10000,
                enableHighAccuracy: true
            };

            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                .then(function(position) {

                    $scope.registro.latitude = position.coords.latitude;
                    $scope.registro.longitude = position.coords.longitude;
                    $scope.registro.precision = position.coords.accuracy;
                    $scope.registro.altitud = position.coords.altitude;

                }, function(err) {
                    //alert('error obtenerCoordenadas: ' + err);
                });
        };

        $scope.insertarRegistro = function() {

            $scope.registro.fecha = $filter('date')(new Date(), 'dd/MM/yyyy');
            $scope.registro.estado = 'ACTIVO';

            /*var datos = [$scope.registro.id_S1, $scope.registro.manzana, $scope.registro.direccion, $scope.registro.latitude, $scope.registro.longitude, $scope.registro.precision, $scope.registro.altitud, $scope.registro.verificado, $scope.registro.predio.id, $scope.registro.jefe_familia, $scope.registro.habitantes, $scope.registro.febriles, $scope.registro.sin_criaderos, $scope.registro.criaderos_eliminados, $scope.registro.criaderos_tratados_fisicamente, $scope.registro.criaderos_tratados_larvicida, $scope.registro.larvicida_A, $scope.registro.larvicida_B, $scope.registro.larvicida_C, $scope.registro.fecha, $scope.registro.estado, false];*/

            var datos = [$scope.registro.id_S1, $scope.registro.manzana, $scope.registro.direccion, $scope.registro.latitude, $scope.registro.longitude, $scope.registro.precision, $scope.registro.altitud, $scope.registro.verificado, $scope.registro.predio.id, $scope.registro.jefe_familia, $scope.registro.habitantes, $scope.registro.febriles, $scope.registro.sin_criaderos, $scope.registro.criaderos_eliminados, $scope.registro.criaderos_tratados_fisicamente, $scope.registro.criaderos_tratados_larvicida, $scope.registro.larvicida_A, $scope.registro.fecha, $scope.registro.estado, false];

            /*var query = 'INSERT INTO RegistrosS1 (id_S1, manzana, direccion, latitude, longitude, precision, altitud, verificado, predio, jefe_familia, habitantes, febriles, sin_criaderos, criaderos_eliminados, criaderos_tratados_fisicamente, criaderos_tratados_larvicida, larvicida_A, larvicida_B, larvicida_C, fecha, estado, enviado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';*/

            var query = 'INSERT INTO RegistrosS1 (id_S1, manzana, direccion, latitude, longitude, precision, altitud, verificado, predio, jefe_familia, habitantes, febriles, sin_criaderos, criaderos_eliminados, criaderos_tratados_fisicamente, criaderos_tratados_larvicida, larvicida_A, fecha, estado, enviado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';            
            
            $cordovaSQLite.execute(db, query, datos).then(
                function(res) {
                
                    RestService.crearRegistro($scope.registro).then(
                    function (response) {
                        //alert("response: " + JSON.stringify(response));
                        if( response.data['respuesta'] > 0){
                            $scope.registro.id = res.insertId;
                            var datos = [response.data['respuesta'], true, $scope.registro.id];
                            var query = 'UPDATE RegistrosS1 SET id_server = ?, enviado = ? WHERE id = ?';
                            $cordovaSQLite.execute(db, query, datos).then(
                            function(res) {
                                
                                /*$scope.registro.id = null;
                                $scope.registro.id_server = null;
                                $scope.registro.id_cA = null;
                                $scope.registro.id_cB = null;
                                $scope.registro.id_cC = null;
                                $scope.registro.manzana = '';
                                $scope.registro.direccion = '';
                                $scope.registro.latitude = null;
                                $scope.registro.longitude = null;
                                $scope.registro.precision = null;
                                $scope.registro.altitud = null;
                                $scope.registro.verificado = false;
                                $scope.registro.predio = null;
                                $scope.registro.jefe_familia = '';
                                $scope.registro.habitantes = null;
                                $scope.registro.febriles = null;
                                $scope.registro.a1 = null;
                                $scope.registro.a2 = null;
                                $scope.registro.a3 = null;
                                $scope.registro.a4 = null;
                                $scope.registro.a5 = null;
                                $scope.registro.a6 = null;
                                $scope.registro.a7 = null;
                                $scope.registro.a8 = null;
                                $scope.registro.a9 = null;
                                $scope.registro.a10 = null;
                                $scope.registro.b1 = null;
                                $scope.registro.b2 = null;
                                $scope.registro.b3 = null;
                                $scope.registro.b4 = null;
                                $scope.registro.b5 = null;
                                $scope.registro.b6 = null;
                                $scope.registro.c1 = null;
                                $scope.registro.c2 = null;
                                $scope.registro.c3 = null;
                                $scope.registro.c4 = null;
                                $scope.registro.sin_criaderos = false;
                                $scope.registro.criaderos_eliminados = null;
                                $scope.registro.criaderos_tratados_fisicamente = null;
                                $scope.registro.criaderos_tratados_larvicida = null;
                                $scope.registro.larvicida_A = null;
                                $scope.registro.larvicida_B = null;
                                $scope.registro.larvicida_C = null;
                                $scope.registro.fecha = '';
                                $scope.registro.estado = '';*/
                                //setTimeout(function() {$location.path('/registros/' + $scope.registro.id_S1)}, 500);
                                
                            }, function(err) {
                                alert('Error Update RegistroS1: ' + JSON.stringify(err));
                                
                            });

                        }
                        
                    }, function (err) {
                        alert( "error: " + JSON.stringify(err));
                        
                        /*$scope.registro.id = null;
                        $scope.registro.id_server = null;
                        $scope.registro.id_cA = null;
                        $scope.registro.id_cB = null;
                        $scope.registro.id_cC = null;
                        $scope.registro.manzana = '';
                        $scope.registro.direccion = '';
                        $scope.registro.latitude = null;
                        $scope.registro.longitude = null;
                        $scope.registro.precision = null;
                        $scope.registro.altitud = null;
                        $scope.registro.verificado = false;
                        $scope.registro.predio = null;
                        $scope.registro.jefe_familia = '';
                        $scope.registro.habitantes = null;
                        $scope.registro.febriles = null;
                        $scope.registro.a1 = null;
                        $scope.registro.a2 = null;
                        $scope.registro.a3 = null;
                        $scope.registro.a4 = null;
                        $scope.registro.a5 = null;
                        $scope.registro.a6 = null;
                        $scope.registro.a7 = null;
                        $scope.registro.a8 = null;
                        $scope.registro.a9 = null;
                        $scope.registro.a10 = null;
                        $scope.registro.b1 = null;
                        $scope.registro.b2 = null;
                        $scope.registro.b3 = null;
                        $scope.registro.b4 = null;
                        $scope.registro.b5 = null;
                        $scope.registro.b6 = null;
                        $scope.registro.c1 = null;
                        $scope.registro.c2 = null;
                        $scope.registro.c3 = null;
                        $scope.registro.c4 = null;
                        $scope.registro.sin_criaderos = false;
                        $scope.registro.criaderos_eliminados = null;
                        $scope.registro.criaderos_tratados_fisicamente = null;
                        $scope.registro.criaderos_tratados_larvicida = null;
                        $scope.registro.larvicida_A = null;
                        $scope.registro.larvicida_B = null;
                        $scope.registro.larvicida_C = null;
                        $scope.registro.fecha = '';
                        $scope.registro.estado = '';*/
                
                    });
                    
                    //setTimeout(function() {$location.path('/registros/' + $scope.registro.id_S1)}, 500);
                              
            }, function(err) {
                alert('Error RegistrosS1: ' + JSON.stringify(err));
            });
            
            //setTimeout(function() {$location.path('/registros/' + $scope.registro.id_S1)}, 500);
            
        };

        $scope.modificarRegistro = function(id) {
            $scope.registro.fecha = $filter('date')(new Date(), 'dd/MM/yyyy');

            /*var datos = [$scope.registro.manzana, $scope.registro.direccion, $scope.registro.verificado, $scope.registro.predio.id, $scope.registro.jefe_familia, $scope.registro.habitantes, $scope.registro.febriles, $scope.registro.sin_criaderos, $scope.registro.criaderos_eliminados, $scope.registro.criaderos_tratados_fisicamente, $scope.registro.criaderos_tratados_larvicida, $scope.registro.larvicida_A, $scope.registro.larvicida_B, $scope.registro.larvicida_C, $scope.registro.fecha, $scope.registro.estado, false, id];*/
            
            var datos = [$scope.registro.manzana, $scope.registro.direccion, $scope.registro.verificado, $scope.registro.predio.id, $scope.registro.jefe_familia, $scope.registro.habitantes, $scope.registro.febriles, $scope.registro.sin_criaderos, $scope.registro.criaderos_eliminados, $scope.registro.criaderos_tratados_fisicamente, $scope.registro.criaderos_tratados_larvicida, $scope.registro.larvicida_A, $scope.registro.fecha, $scope.registro.estado, false, id];

            /*var query = 'UPDATE RegistrosS1 SET manzana = ?, direccion = ?, verificado = ?, predio = ?, jefe_familia = ?, habitantes = ?, febriles = ?, sin_criaderos = ?, criaderos_eliminados = ?, criaderos_tratados_fisicamente = ?, criaderos_tratados_larvicida = ?, larvicida_A = ?, larvicida_B = ?, larvicida_C = ?, fecha = ?, estado = ?, enviado = ? WHERE id = ?';*/
            
            var query = 'UPDATE RegistrosS1 SET manzana = ?, direccion = ?, verificado = ?, predio = ?, jefe_familia = ?, habitantes = ?, febriles = ?, sin_criaderos = ?, criaderos_eliminados = ?, criaderos_tratados_fisicamente = ?, criaderos_tratados_larvicida = ?, larvicida_A = ?, fecha = ?, estado = ?, enviado = ? WHERE id = ?';

            $cordovaSQLite.execute(db, query, datos).then(
                function(res) {
                    RestService.modificarRegistro($scope.registro).then(
                    function (response) {

                        if( response.data['respuesta'] == "OK"){

                                var datos = [true, id];
                                var query = 'UPDATE RegistrosS1 SET enviado = ? WHERE id = ?';
                                $cordovaSQLite.execute(db, query, datos).then(
                                function(res) {
                                    
                                    /*$scope.registro.id = null;
                                    $scope.registro.id_cA = null;
                                    $scope.registro.id_cB = null;
                                    $scope.registro.id_cC = null;
                                    $scope.registro.manzana = '';
                                    $scope.registro.direccion = '';
                                    $scope.registro.latitude = null;
                                    $scope.registro.longitude = null;
                                    $scope.registro.precision = null;
                                    $scope.registro.altitud = null;
                                    $scope.registro.verificado = false;
                                    $scope.registro.predio = null;
                                    $scope.registro.jefe_familia = '';
                                    $scope.registro.habitantes = null;
                                    $scope.registro.febriles = null;
                                    $scope.registro.a1 = null;
                                    $scope.registro.a2 = null;
                                    $scope.registro.a3 = null;
                                    $scope.registro.a4 = null;
                                    $scope.registro.a5 = null;
                                    $scope.registro.a6 = null;
                                    $scope.registro.a7 = null;
                                    $scope.registro.a8 = null;
                                    $scope.registro.a9 = null;
                                    $scope.registro.a10 = null;
                                    $scope.registro.b1 = null;
                                    $scope.registro.b2 = null;
                                    $scope.registro.b3 = null;
                                    $scope.registro.b4 = null;
                                    $scope.registro.b5 = null;
                                    $scope.registro.b6 = null;
                                    $scope.registro.c1 = null;
                                    $scope.registro.c2 = null;
                                    $scope.registro.c3 = null;
                                    $scope.registro.c4 = null;
                                    $scope.registro.sin_criaderos = false;
                                    $scope.registro.criaderos_eliminados = null;
                                    $scope.registro.criaderos_tratados_fisicamente = null;
                                    $scope.registro.criaderos_tratados_larvicida = null;
                                    $scope.registro.larvicida_A = null;
                                    $scope.registro.larvicida_B = null;
                                    $scope.registro.larvicida_C = null;
                                    $scope.registro.fecha = '';
                                    $scope.registro.estado = '';*/
                                    
                                }, function(err) {
                                    alert('Error Update RegistroS1: ' + JSON.stringify(err));
                                }); 
                        }
                        
                        //setTimeout(function() {$location.path('/registros/' + $scope.registro.id_S1)}, 500);

                    }, function (err) {
                        alert( "error: " + JSON.stringify(err));
                    });
                
            }, function(err) {
                alert('Error RegistrosS1: ' + JSON.stringify(err));
                
                /*$scope.registro.id = null;
                $scope.registro.id_cA = null;
                $scope.registro.id_cB = null;
                $scope.registro.id_cC = null;
                $scope.registro.manzana = '';
                $scope.registro.direccion = '';
                $scope.registro.latitude = null;
                $scope.registro.longitude = null;
                $scope.registro.precision = null;
                $scope.registro.altitud = null;
                $scope.registro.verificado = false;
                $scope.registro.predio = null;
                $scope.registro.jefe_familia = '';
                $scope.registro.habitantes = null;
                $scope.registro.febriles = null;
                $scope.registro.a1 = null;
                $scope.registro.a2 = null;
                $scope.registro.a3 = null;
                $scope.registro.a4 = null;
                $scope.registro.a5 = null;
                $scope.registro.a6 = null;
                $scope.registro.a7 = null;
                $scope.registro.a8 = null;
                $scope.registro.a9 = null;
                $scope.registro.a10 = null;
                $scope.registro.b1 = null;
                $scope.registro.b2 = null;
                $scope.registro.b3 = null;
                $scope.registro.b4 = null;
                $scope.registro.b5 = null;
                $scope.registro.b6 = null;
                $scope.registro.c1 = null;
                $scope.registro.c2 = null;
                $scope.registro.c3 = null;
                $scope.registro.c4 = null;
                $scope.registro.sin_criaderos = false;
                $scope.registro.criaderos_eliminados = null;
                $scope.registro.criaderos_tratados_fisicamente = null;
                $scope.registro.criaderos_tratados_larvicida = null;
                $scope.registro.larvicida_A = null;
                $scope.registro.larvicida_B = null;
                $scope.registro.larvicida_C = null;
                $scope.registro.fecha = '';
                $scope.registro.estado = '';
                
                $location.path('/registros/' + $scope.registro.id_S1);*/
            });

        }

        $scope.showGuardando = function() {
            $ionicLoading.show({
                template: 'Guardando...'
            });
            //setTimeout(function() {$location.path('/registros/' + $scope.registro.id_S1)}, 500);
        };

        $scope.hideGuardando = function() {
            $ionicLoading.hide();
        };
        
        $scope.inicializarRegistro = function() {
            $scope.registro.id = null;
            $scope.registro.id_server = null;
            $scope.registro.id_cA = null;
            $scope.registro.id_cB = null;
            $scope.registro.id_cC = null;
            $scope.registro.manzana = '';
            $scope.registro.direccion = '';
            $scope.registro.latitude = null;
            $scope.registro.longitude = null;
            $scope.registro.precision = null;
            $scope.registro.altitud = null;
            $scope.registro.verificado = false;
            $scope.registro.predio = null;
            $scope.registro.jefe_familia = '';
            $scope.registro.habitantes = null;
            $scope.registro.febriles = null;
            $scope.registro.a1 = null;
            $scope.registro.a2 = null;
            $scope.registro.a3 = null;
            $scope.registro.a4 = null;
            $scope.registro.a5 = null;
            $scope.registro.a6 = null;
            $scope.registro.a7 = null;
            $scope.registro.a8 = null;
            $scope.registro.a9 = null;
            $scope.registro.a10 = null;
            $scope.registro.b1 = null;
            $scope.registro.b2 = null;
            $scope.registro.b3 = null;
            $scope.registro.b4 = null;
            $scope.registro.b5 = null;
            $scope.registro.b6 = null;
            $scope.registro.c1 = null;
            $scope.registro.c2 = null;
            $scope.registro.c3 = null;
            $scope.registro.c4 = null;
            $scope.registro.sin_criaderos = false;
            $scope.registro.criaderos_eliminados = null;
            $scope.registro.criaderos_tratados_fisicamente = null;
            $scope.registro.criaderos_tratados_larvicida = null;
            $scope.registro.larvicida_A = null;
            //$scope.registro.larvicida_B = null;
            //$scope.registro.larvicida_C = null;
            $scope.registro.fecha = '';
            $scope.registro.estado = '';    
        };

        $scope.guardar = function() {
            $scope.validarDatos();
            if($scope.sw === true){
                
                if (window.cordova) {
                    cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
                        //alert("Location is " + (enabled ? "enabled" : "disabled"));
                        if(enabled == false){
                            alert("Active el GPS");
                            //$scope.sw = false;
                        }
                        else if ($scope.registro.id == null) {

                            var fecha = $filter('date')(new Date(), 'dd/MM/yyyy');
                            var datos = [$scope.registro.manzana, $scope.registro.direccion, $scope.registro.predio.id, fecha, "ACTIVO"];

                            var query = 'SELECT * FROM RegistrosS1 WHERE manzana = ? and direccion = ? and predio = ? and fecha = ? and estado = ?';

                            $cordovaSQLite.execute(db, query, datos).then(
                            function(res) {
                                    if(res.rows.length <= 0){
                                        $scope.showGuardando();
                                        $scope.obtenerCoordenadas();
                                        setTimeout(function() {
                                            $scope.insertarRegistro();
                                            $scope.hideGuardando();
                                            $scope.showAlert();
                                        }, 10000);                                    
                                }else{
                                    alert("La planilla ya existe");    
                                }

                            }, function(err) {
                                alert('Error verificar Insertar RegistroS1: ' + err);
                            });

                        } else {
                            $scope.showGuardando();
                            $scope.modificarRegistro($scope.registro.id);               
                            $scope.hideGuardando();
                            $scope.showAlert();
                        }    
                        
                    }, function(error) {
                        alert("Error al verificar GPS: " + error);
                    });
                };
                
                /*if ($scope.registro.id == null) {
                    
                    var fecha = $filter('date')(new Date(), 'dd/MM/yyyy');
                    var datos = [$scope.registro.manzana, $scope.registro.direccion, $scope.registro.predio.id, fecha, "ACTIVO"];

                    var query = 'SELECT * FROM RegistrosS1 WHERE manzana = ? and direccion = ? and predio = ? and fecha = ? and estado = ?';

                    $cordovaSQLite.execute(db, query, datos).then(
                    function(res) {
                        if(res.rows.length <= 0){
                            $scope.showGuardando();
                            $scope.obtenerCoordenadas();
                            setTimeout(function() {
                                $scope.insertarRegistro();
                                $scope.hideGuardando();
                                $scope.showAlert();
                            }, 10000);
                            
                        }else{
                            alert("El registro ya existe");    
                        }

                    }, function(err) {
                        alert('Error verificar Insertar RegistroS1: ' + err);
                    });

                } else {
                    $scope.showGuardando();
                    $scope.modificarRegistro($scope.registro.id);               
                    $scope.hideGuardando();
                    $scope.showAlert();
                }*/
            }
        };
        
        $scope.validarDatos = function() {
            $scope.sw = true;
            
            if($scope.registro.manzana === ''){
                alert("Ingrese el código de Manzana");
                $scope.sw = false;

            }else if($scope.registro.direccion === ''){
                alert("Ingrese la Dirección");
                $scope.sw = false;
                
            }
            else if($scope.registro.predio === null){
                alert("Ingrese el Tipo de Predio");
                $scope.sw = false;
                
            }
        };
        
        $scope.validarVerificado = function() {
            //alert("predio: " + JSON.stringify($scope.registro.predio) );
            if( $scope.registro.predio.nombre == "Cerrado" || $scope.registro.predio.nombre == "cerrado" ){
                $scope.registro.verificado = false;
                //alert("verificado: " +  $scope.registro.verificado);
            }
            else if( $scope.registro.predio.nombre == "Renuente" || $scope.registro.predio.nombre == "renuente" ){
                $scope.registro.verificado = false;
                //alert("verificado: " +  $scope.registro.verificado);    
            }
        };
        
        
    }, false);
    
    $ionicPlatform.registerBackButtonAction(function() {
        //var path = $location.path()
        //alert("state: " + $state.current.name);
        if ($state.current.name === "step1") {
            $scope.showConfirm(); 
            //$ionicHistory.goBack();
        }
    }, 100);


})

.controller('Step1FormCtrl', function($scope, $rootScope, $state,
    $location, $ionicPopup, $cordovaSQLite,
    $filter, $cordovaGeolocation, $ionicLoading, RestService) {

    var validate = $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        if (($scope.step1Form.$invalid) && (toState.data.step > fromState.data.step))
            event.preventDefault();
    });

    $scope.$on('$destroy', validate);

    // An alert dialog
    $scope.showAlert = function() {
        var alertPopup = $ionicPopup.alert({
            title: 'Info',
            template: 'La operación se realizó con éxito'
        })
    };

    document.addEventListener('deviceready', function() {

        var db = window.sqlitePlugin.openDatabase({
            name: 'my.db',
            location: 'default'
        });

        $scope.selectListaPredio = function() {
            $scope.list_predio = [];

            var query = 'SELECT id, nombre FROM TiposPredio';
            $cordovaSQLite.execute(db, query, []).then(function(res) {
                if (res.rows.length > 0) {
                    for (var i = 0; i < res.rows.length; i++) {
                        $scope.list_predio.push(res.rows.item(i));
                        if ($scope.list_predio[i].id == $scope.registro.predio) {
                            $scope.registro.predio = $scope.list_predio[i];
                        }
                    }
                } else {
                    //alert('No results found');
                }
            }, function(err) {
                alert('Error Lista TiposPredio: ' + err);
            });
                   
        };
        $scope.selectListaPredio();

    }, false);

})

.controller('Step2Ctrl', function($scope, $ionicPopup, $location, $ionicPlatform, $state) {
    $scope.step2Submitted = false;

    $scope.submit = function() {
        $scope.step2Submitted = true;
    };
    
    $scope.showConfirm = function() {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Cancelar',
            template: 'Desea cancelar la operación?'
        });

        confirmPopup.then(function(res) {
            if (res) {
                $scope.cancelar();
            }
        });
    };

    $scope.cancelar = function() {
        
        $scope.registro.id = null;
        $scope.registro.id_cA = null;
        $scope.registro.id_cB = null;
        $scope.registro.id_cC = null;
        $scope.registro.manzana = '';
        $scope.registro.direccion = '';
        //$scope.registro.coordenada =  '';
        $scope.registro.latitude = null;
        $scope.registro.longitude = null;
        $scope.registro.precision = null;
        $scope.registro.altitud = null;
        $scope.registro.verificado = false;
        $scope.registro.predio = null;
        $scope.registro.jefe_familia = '';
        $scope.registro.habitantes = null;
        $scope.registro.febriles = null;
        $scope.registro.a1 = null;
        $scope.registro.a2 = null;
        $scope.registro.a3 = null;
        $scope.registro.a4 = null;
        $scope.registro.a5 = null;
        $scope.registro.a6 = null;
        $scope.registro.a7 = null;
        $scope.registro.a8 = null;
        $scope.registro.a9 = null;
        $scope.registro.a10 = null;
        $scope.registro.b1 = null;
        $scope.registro.b2 = null;
        $scope.registro.b3 = null;
        $scope.registro.b4 = null;
        $scope.registro.b5 = null;
        $scope.registro.b6 = null;
        $scope.registro.c1 = null;
        $scope.registro.c2 = null;
        $scope.registro.c3 = null;
        $scope.registro.c4 = null;
        $scope.registro.sin_criaderos = false;
        $scope.registro.criaderos_eliminados = null;
        $scope.registro.criaderos_tratados_fisicamente = null;
        $scope.registro.criaderos_tratados_larvicida = null;
        $scope.registro.larvicida_A = null;
        //$scope.registro.larvicida_B = null;
        //$scope.registro.larvicida_C = null;
        $scope.registro.fecha = '';
        $scope.registro.estado = '';
        $location.path('/registros/' + $scope.registro.id_S1);
    };
    
    $ionicPlatform.registerBackButtonAction(function() {
        //var path = $location.path()
        //alert("state: " + $state.current.name);
        if ($state.current.name === "step2") {
            $scope.showConfirm(); 
            //$ionicHistory.goBack();
        }
    }, 100);
})

.controller('Step2FormCtrl', function($scope, $rootScope, $state) {
    var validate = $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        if (($scope.step2Form.$invalid) && (toState.data.step > fromState.data.step))
            event.preventDefault();
    });

    $scope.$on('$destroy', validate);
})

.controller('Step3Ctrl', function($scope, $ionicPopup, $location, $ionicPlatform, $state) {
    $scope.step3Submitted = false;

    $scope.submit = function() {
        $scope.step3Submitted = true;
    };
    
    $scope.showConfirm = function() {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Cancelar',
            template: 'Desea cancelar la operación?'
        });

        confirmPopup.then(function(res) {
            if (res) {
                $scope.cancelar();
            }
        });
    };

    $scope.cancelar = function() {
        
        $scope.registro.id = null;
        $scope.registro.id_cA = null;
        $scope.registro.id_cB = null;
        $scope.registro.id_cC = null;
        $scope.registro.manzana = '';
        $scope.registro.direccion = '';
        //$scope.registro.coordenada =  '';
        $scope.registro.latitude = null;
        $scope.registro.longitude = null;
        $scope.registro.precision = null;
        $scope.registro.altitud = null;
        $scope.registro.verificado = false;
        $scope.registro.predio = null;
        $scope.registro.jefe_familia = '';
        $scope.registro.habitantes = null;
        $scope.registro.febriles = null;
        $scope.registro.a1 = null;
        $scope.registro.a2 = null;
        $scope.registro.a3 = null;
        $scope.registro.a4 = null;
        $scope.registro.a5 = null;
        $scope.registro.a6 = null;
        $scope.registro.a7 = null;
        $scope.registro.a8 = null;
        $scope.registro.a9 = null;
        $scope.registro.a10 = null;
        $scope.registro.b1 = null;
        $scope.registro.b2 = null;
        $scope.registro.b3 = null;
        $scope.registro.b4 = null;
        $scope.registro.b5 = null;
        $scope.registro.b6 = null;
        $scope.registro.c1 = null;
        $scope.registro.c2 = null;
        $scope.registro.c3 = null;
        $scope.registro.c4 = null;
        $scope.registro.sin_criaderos = false;
        $scope.registro.criaderos_eliminados = null;
        $scope.registro.criaderos_tratados_fisicamente = null;
        $scope.registro.criaderos_tratados_larvicida = null;
        $scope.registro.larvicida_A = null;
        //$scope.registro.larvicida_B = null;
        //$scope.registro.larvicida_C = null;
        $scope.registro.fecha = '';
        $scope.registro.estado = '';
        $location.path('/registros/' + $scope.registro.id_S1);
    };
    
    $ionicPlatform.registerBackButtonAction(function() {
        //var path = $location.path()
        //alert("state: " + $state.current.name);
        if ($state.current.name === "step3") {
            $scope.showConfirm(); 
            //$ionicHistory.goBack();
        }
    }, 100);

})

.controller('Step3FormCtrl', function($scope, $rootScope, $timeout) {
    /*$timeout(function() {
      $scope.step3Form.tos.$setValidity('agree', $scope.data.tos);
    });

    $scope.checkTos = function() {
      $scope.step3Form.tos.$setValidity('agree', $scope.data.tos);
    }*/

    var validate = $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        if (($scope.step3Form.$invalid) && (toState.data.step > fromState.data.step))
            event.preventDefault();
    });

    $scope.$on('$destroy', validate);
})

.controller('Step4Ctrl', function($scope, $ionicPopup, $location, $ionicPlatform, $state) {
    $scope.step4Submitted = false;

    $scope.submit = function() {
        $scope.step4Submitted = true;
    };

    $scope.showConfirm = function() {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Cancelar',
            template: 'Desea cancelar la operación?'
        });

        confirmPopup.then(function(res) {
            if (res) {
                $scope.cancelar();
            }
        });
    };

    $scope.cancelar = function() {
        
        $scope.registro.id = null;
        $scope.registro.id_cA = null;
        $scope.registro.id_cB = null;
        $scope.registro.id_cC = null;
        $scope.registro.manzana = '';
        $scope.registro.direccion = '';
        //$scope.registro.coordenada =  '';
        $scope.registro.latitude = null;
        $scope.registro.longitude = null;
        $scope.registro.precision = null;
        $scope.registro.altitud = null;
        $scope.registro.verificado = false;
        $scope.registro.predio = null;
        $scope.registro.jefe_familia = '';
        $scope.registro.habitantes = null;
        $scope.registro.febriles = null;
        $scope.registro.a1 = null;
        $scope.registro.a2 = null;
        $scope.registro.a3 = null;
        $scope.registro.a4 = null;
        $scope.registro.a5 = null;
        $scope.registro.a6 = null;
        $scope.registro.a7 = null;
        $scope.registro.a8 = null;
        $scope.registro.a9 = null;
        $scope.registro.a10 = null;
        $scope.registro.b1 = null;
        $scope.registro.b2 = null;
        $scope.registro.b3 = null;
        $scope.registro.b4 = null;
        $scope.registro.b5 = null;
        $scope.registro.b6 = null;
        $scope.registro.c1 = null;
        $scope.registro.c2 = null;
        $scope.registro.c3 = null;
        $scope.registro.c4 = null;
        $scope.registro.sin_criaderos = false;
        $scope.registro.criaderos_eliminados = null;
        $scope.registro.criaderos_tratados_fisicamente = null;
        $scope.registro.criaderos_tratados_larvicida = null;
        $scope.registro.larvicida_A = null;
        //$scope.registro.larvicida_B = null;
        //$scope.registro.larvicida_C = null;
        $scope.registro.fecha = '';
        $scope.registro.estado = '';
        $location.path('/registros/' + $scope.registro.id_S1);
    };
    
    $ionicPlatform.registerBackButtonAction(function() {
        //var path = $location.path()
        //alert("state: " + $state.current.name);
        if ($state.current.name === "step4") {
            $scope.showConfirm(); 
            //$ionicHistory.goBack();
        }
    }, 100);    
    
})

.controller('Step4FormCtrl', function($scope, $rootScope, $timeout) {

    var validate = $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        if (($scope.step4Form.$invalid) && (toState.data.step > fromState.data.step))
            event.preventDefault();
    });

    $scope.$on('$destroy', validate);
})

.controller('Step5Ctrl', function($scope, $ionicPopup, $location, $ionicPlatform, $state) {
    $scope.step5Submitted = false;

    $scope.submit = function() {
        $scope.step5Submitted = true;
    };

    $scope.showConfirm = function() {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Cancelar',
            template: 'Desea cancelar la operación?'
        });

        confirmPopup.then(function(res) {
            if (res) {
                $scope.cancelar();
            }
        });
    };

    $scope.cancelar = function() {
        
        $scope.registro.id = null;
        $scope.registro.id_cA = null;
        $scope.registro.id_cB = null;
        $scope.registro.id_cC = null;
        $scope.registro.manzana = '';
        $scope.registro.direccion = '';
        //$scope.registro.coordenada =  '';
        $scope.registro.latitude = null;
        $scope.registro.longitude = null;
        $scope.registro.precision = null;
        $scope.registro.altitud = null;
        $scope.registro.verificado = false;
        $scope.registro.predio = null;
        $scope.registro.jefe_familia = '';
        $scope.registro.habitantes = null;
        $scope.registro.febriles = null;
        $scope.registro.a1 = null;
        $scope.registro.a2 = null;
        $scope.registro.a3 = null;
        $scope.registro.a4 = null;
        $scope.registro.a5 = null;
        $scope.registro.a6 = null;
        $scope.registro.a7 = null;
        $scope.registro.a8 = null;
        $scope.registro.a9 = null;
        $scope.registro.a10 = null;
        $scope.registro.b1 = null;
        $scope.registro.b2 = null;
        $scope.registro.b3 = null;
        $scope.registro.b4 = null;
        $scope.registro.b5 = null;
        $scope.registro.b6 = null;
        $scope.registro.c1 = null;
        $scope.registro.c2 = null;
        $scope.registro.c3 = null;
        $scope.registro.c4 = null;
        $scope.registro.sin_criaderos = false;
        $scope.registro.criaderos_eliminados = null;
        $scope.registro.criaderos_tratados_fisicamente = null;
        $scope.registro.criaderos_tratados_larvicida = null;
        $scope.registro.larvicida_A = null;
        //$scope.registro.larvicida_B = null;
        //$scope.registro.larvicida_C = null;
        $scope.registro.fecha = '';
        $scope.registro.estado = '';
        $location.path('/registros/' + $scope.registro.id_S1);
    };
    
    $ionicPlatform.registerBackButtonAction(function() {
        //var path = $location.path()
        //alert("state: " + $state.current.name);
        if ($state.current.name === "step5") {
            $scope.showConfirm(); 
            //$ionicHistory.goBack();
        }
    }, 100);    
    
})

.controller('Step5FormCtrl', function($scope, $rootScope, $timeout) {

    //$scope.sw = false;
    $scope.registro.sin_criaderos = true;
    
    var validate = $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        if (($scope.step5Form.$invalid) && (toState.data.step > fromState.data.step))
            event.preventDefault();
    });

    $scope.$on('$destroy', validate);
    
    $scope.validarCampos = function() {
        
        /*$scope.registro.id = null;
        $scope.registro.id_cA = null;
        $scope.registro.id_cB = null;
        $scope.registro.id_cC = null;
        $scope.registro.manzana = '';
        $scope.registro.direccion = '';
        //$scope.registro.coordenada =  '';
        $scope.registro.latitude = null;
        $scope.registro.longitude = null;
        $scope.registro.precision = null;
        $scope.registro.altitud = null;
        $scope.registro.verificado = false;
        $scope.registro.predio = null;
        $scope.registro.jefe_familia = '';
        $scope.registro.habitantes = null;
        $scope.registro.febriles = null;
        $scope.registro.a1 = null;
        $scope.registro.a2 = null;
        $scope.registro.a3 = null;
        $scope.registro.a4 = null;
        $scope.registro.a5 = null;
        $scope.registro.a6 = null;
        $scope.registro.a7 = null;
        $scope.registro.a8 = null;
        $scope.registro.a9 = null;
        $scope.registro.a10 = null;
        $scope.registro.b1 = null;
        $scope.registro.b2 = null;
        $scope.registro.b3 = null;
        $scope.registro.b4 = null;
        $scope.registro.b5 = null;
        $scope.registro.b6 = null;
        $scope.registro.c1 = null;
        $scope.registro.c2 = null;
        $scope.registro.c3 = null;
        $scope.registro.c4 = null;
        $scope.registro.sin_criaderos = false;
        $scope.registro.criaderos_eliminados = null;
        $scope.registro.criaderos_tratados_fisicamente = null;
        $scope.registro.criaderos_tratados_larvicida = null;
        $scope.registro.larvicida_A = null;
        //$scope.registro.larvicida_B = null;
        //$scope.registro.larvicida_C = null;
        $scope.registro.fecha = '';
        $scope.registro.estado = '';
        $location.path('/registros/' + $scope.registro.id_S1);*/
        
        /*$scope.registro.jefe_familia != '' && $scope.registro.jefe_familia != null ||
            $scope.registro.habitantes != '' && $scope.registro.habitantes != null ||
            $scope.registro.febriles != '' && $scope.registro.febriles != null ||*/
        
        if( $scope.registro.a1 != '' && $scope.registro.a1 != null ||
            $scope.registro.a2 != '' && $scope.registro.a2 != null ||
            $scope.registro.a3 != '' && $scope.registro.a3 != null ||
            $scope.registro.a4 != '' && $scope.registro.a4 != null ||
            $scope.registro.a5 != '' && $scope.registro.a5 != null ||
            $scope.registro.a6 != '' && $scope.registro.a6 != null ||
            $scope.registro.a7 != '' && $scope.registro.a7 != null ||
            $scope.registro.a8 != '' && $scope.registro.a8 != null ||
            $scope.registro.a9 != '' && $scope.registro.a9 != null ||
            $scope.registro.a10 != '' && $scope.registro.a10 != null ||
            $scope.registro.b1 != '' && $scope.registro.b1 != null ||
            $scope.registro.b2 != '' && $scope.registro.b2 != null ||
            $scope.registro.b3 != '' && $scope.registro.b3 != null ||
            $scope.registro.b4 != '' && $scope.registro.b4 != null ||
            $scope.registro.b5 != '' && $scope.registro.b5 != null ||
            $scope.registro.b6 != '' && $scope.registro.b6 != null ||
            $scope.registro.c1 != '' && $scope.registro.c1 != null ||
            $scope.registro.c2 != '' && $scope.registro.c2 != null ||
            $scope.registro.c3 != '' && $scope.registro.c3 != null ||
            $scope.registro.c4 != '' && $scope.registro.c4 != null ){
            
            //alert("registro: " + JSON.stringify($scope.registro));
            $scope.registro.sin_criaderos = false;
            
        }
    };
    
    $scope.validarCampos();
    
})

.controller('DoneCtrl', function($scope, $rootScope, $ionicHistory,
    $location, $ionicPopup, $cordovaSQLite,
    $filter, $cordovaGeolocation, $ionicLoading, RestService, $ionicPlatform, $state) {

    document.addEventListener('deviceready', function() {

        var db = window.sqlitePlugin.openDatabase({
            name: 'my.db',
            location: 'default'
        });

        $scope.obtenerCoordenadas = function() {
            var posOptions = {
                timeout: 10000,
                maximumAge: 10000,
                enableHighAccuracy: true
            };

            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                .then(function(position) {

                    $scope.registro.latitude = position.coords.latitude;
                    $scope.registro.longitude = position.coords.longitude;
                    $scope.registro.precision = position.coords.accuracy;
                    $scope.registro.altitud = position.coords.altitude;

                }, function(err) {
                    //alert('error obtenerCoordenadas: ' + err);
                });
        };

        // An alert dialog
        $scope.showAlert = function() {
            var alertPopup = $ionicPopup.alert({
                title: 'Info',
                template: 'La operación se realizó con éxito'
            });
            alertPopup.then(function(res) {
                if (res) {
                    //setTimeout(function() {$location.path('/registros/' + $scope.registro.id_S1)}, 500);
                    $location.path('/registros/' + $scope.registro.id_S1);
                    $scope.inicializarRegistro();
                }
            });
            
        };

        $scope.showConfirm = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Cancelar',
                template: 'Desea cancelar la operación?'
            });

            confirmPopup.then(function(res) {
                if (res) {
                    $scope.cancelar();
                }
            });
        };

        $scope.cancelar = function() {
            
            $scope.registro.id = null;
            $scope.registro.id_cA = null;
            $scope.registro.id_cB = null;
            $scope.registro.id_cC = null;
            $scope.registro.manzana = '';
            $scope.registro.direccion = '';
            $scope.registro.latitude = null;
            $scope.registro.longitude = null;
            $scope.registro.precision = null;
            $scope.registro.altitud = null;
            $scope.registro.verificado = false;
            $scope.registro.predio = null;
            $scope.registro.jefe_familia = '';
            $scope.registro.habitantes = null;
            $scope.registro.febriles = null;
            $scope.registro.a1 = null;
            $scope.registro.a2 = null;
            $scope.registro.a3 = null;
            $scope.registro.a4 = null;
            $scope.registro.a5 = null;
            $scope.registro.a6 = null;
            $scope.registro.a7 = null;
            $scope.registro.a8 = null;
            $scope.registro.a9 = null;
            $scope.registro.a10 = null;
            $scope.registro.b1 = null;
            $scope.registro.b2 = null;
            $scope.registro.b3 = null;
            $scope.registro.b4 = null;
            $scope.registro.b5 = null;
            $scope.registro.b6 = null;
            $scope.registro.c1 = null;
            $scope.registro.c2 = null;
            $scope.registro.c3 = null;
            $scope.registro.c4 = null;
            $scope.registro.sin_criaderos = false;
            $scope.registro.criaderos_eliminados = null;
            $scope.registro.criaderos_tratados_fisicamente = null;
            $scope.registro.criaderos_tratados_larvicida = null;
            $scope.registro.larvicida_A = null;
            //$scope.registro.larvicida_B = null;
            //$scope.registro.larvicida_C = null;
            $scope.registro.fecha = '';
            $scope.registro.estado = '';
            $location.path('/registros/' + $scope.registro.id_S1);
        };

        $scope.insertarRegistro = function() {

            $scope.registro.fecha = $filter('date')(new Date(), 'dd/MM/yyyy');
            $scope.registro.estado = 'ACTIVO';
            
            /*var datos = [$scope.registro.id_S1, $scope.registro.manzana, $scope.registro.direccion, $scope.registro.latitude, $scope.registro.longitude, $scope.registro.precision, $scope.registro.altitud, $scope.registro.verificado, $scope.registro.predio.id, $scope.registro.jefe_familia, $scope.registro.habitantes, $scope.registro.febriles, $scope.registro.sin_criaderos, $scope.registro.criaderos_eliminados, $scope.registro.criaderos_tratados_fisicamente, $scope.registro.criaderos_tratados_larvicida, $scope.registro.larvicida_A, $scope.registro.larvicida_B, $scope.registro.larvicida_C, $scope.registro.fecha, $scope.registro.estado, false];*/
            
            var datos = [$scope.registro.id_S1, $scope.registro.manzana, $scope.registro.direccion, $scope.registro.latitude, $scope.registro.longitude, $scope.registro.precision, $scope.registro.altitud, $scope.registro.verificado, $scope.registro.predio.id, $scope.registro.jefe_familia, $scope.registro.habitantes, $scope.registro.febriles, $scope.registro.sin_criaderos, $scope.registro.criaderos_eliminados, $scope.registro.criaderos_tratados_fisicamente, $scope.registro.criaderos_tratados_larvicida, $scope.registro.larvicida_A, $scope.registro.fecha, $scope.registro.estado, false];

            db.transaction(function(tx) {

                /*var query = 'INSERT INTO RegistrosS1 (id_S1, manzana, direccion, latitude, longitude, precision, altitud, verificado, predio, jefe_familia, habitantes, febriles, sin_criaderos, criaderos_eliminados, criaderos_tratados_fisicamente, criaderos_tratados_larvicida, larvicida_A, larvicida_B, larvicida_C, fecha, estado, enviado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';*/
                
                var query = 'INSERT INTO RegistrosS1 (id_S1, manzana, direccion, latitude, longitude, precision, altitud, verificado, predio, jefe_familia, habitantes, febriles, sin_criaderos, criaderos_eliminados, criaderos_tratados_fisicamente, criaderos_tratados_larvicida, larvicida_A, fecha, estado, enviado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
                tx.executeSql(query, datos, function(tx, res) {

                    $scope.registro.id = res.insertId;
                    var datos = null;
                    if($scope.registro.a1!=null || $scope.registro.a2!=null || $scope.registro.a3!=null || $scope.registro.a4!=null ||
                      $scope.registro.a5!=null || $scope.registro.a6!=null || $scope.registro.a7!=null || $scope.registro.a8!=null ||
                      $scope.registro.a9!=null || $scope.registro.a10!=null){
                        var datos = [$scope.registro.id, $scope.registro.a1, $scope.registro.a2, $scope.registro.a3, $scope.registro.a4, $scope.registro.a5, $scope.registro.a6, $scope.registro.a7, $scope.registro.a8, $scope.registro.a9, $scope.registro.a10];
                        //alert("Criadero A: " + JSON.stringify($scope.registro));       
                        var query = 'INSERT INTO CriaderosA (id_registro_S1, criaderos_A1, criaderos_A2, criaderos_A3, criaderos_A4, criaderos_A5, criaderos_A6, criaderos_A7, criaderos_A8, criaderos_A9, criaderos_A10) VALUES (?,?,?,?,?,?,?,?,?,?,?)';
                        tx.executeSql(query, datos, function(tx, res) {
                            //alert('INSERT ID A-> ' + res.insertId);
                        });
                    }

                    var datos = null;
                    if($scope.registro.b1!=null || $scope.registro.b2!=null || $scope.registro.b3!=null ||
                      $scope.registro.b4!=null || $scope.registro.b5!=null || $scope.registro.b6!=null){
                        var datos = [$scope.registro.id, $scope.registro.b1, $scope.registro.b2, $scope.registro.b3, $scope.registro.b4, $scope.registro.b5, $scope.registro.b6];

                        var query = 'INSERT INTO CriaderosB (id_registro_S1, criaderos_B1, criaderos_B2, criaderos_B3, criaderos_B4, criaderos_B5, criaderos_B6) VALUES (?,?,?,?,?,?,?)';
                        tx.executeSql(query, datos, function(tx, res) {
                            //alert('INSERT ID B-> ' + res.insertId);
                        });
                    }

                    var datos = null;
                    if($scope.registro.c1!=null || $scope.registro.c2!=null || $scope.registro.c3!=null || $scope.registro.c4!=null){
                        var datos = [$scope.registro.id, $scope.registro.c1, $scope.registro.c2, $scope.registro.c3, $scope.registro.c4];

                        var query = 'INSERT INTO CriaderosC (id_registro_S1, criaderos_C1, criaderos_C2, criaderos_C3, criaderos_C4) VALUES (?,?,?,?,?)';
                        tx.executeSql(query, datos, function(tx, res) {
                            //alert('INSERT ID C-> ' + res.insertId);
                        });
                    }
                    
                    RestService.crearRegistro($scope.registro).then(
                    function (response) {
   
                        if( response.data['respuesta'] > 0){
                            $scope.registro.id = res.insertId;
                            var datos = [response.data['respuesta'], true, $scope.registro.id];
                            var query = 'UPDATE RegistrosS1 SET id_server = ?, enviado = ? WHERE id = ?';
                            $cordovaSQLite.execute(db, query, datos).then(
                            function(res) {
                                
                                /*$scope.registro.id = null;
                                $scope.registro.id_cA = null;
                                $scope.registro.id_cB = null;
                                $scope.registro.id_cC = null;
                                $scope.registro.manzana = '';
                                $scope.registro.direccion = '';
                                $scope.registro.latitude = null;
                                $scope.registro.longitude = null;
                                $scope.registro.precision = null;
                                $scope.registro.altitud = null;
                                $scope.registro.verificado = false;
                                $scope.registro.predio = null;
                                $scope.registro.jefe_familia = '';
                                $scope.registro.habitantes = null;
                                $scope.registro.febriles = null;
                                $scope.registro.a1 = null;
                                $scope.registro.a2 = null;
                                $scope.registro.a3 = null;
                                $scope.registro.a4 = null;
                                $scope.registro.a5 = null;
                                $scope.registro.a6 = null;
                                $scope.registro.a7 = null;
                                $scope.registro.a8 = null;
                                $scope.registro.a9 = null;
                                $scope.registro.a10 = null;
                                $scope.registro.b1 = null;
                                $scope.registro.b2 = null;
                                $scope.registro.b3 = null;
                                $scope.registro.b4 = null;
                                $scope.registro.b5 = null;
                                $scope.registro.b6 = null;
                                $scope.registro.c1 = null;
                                $scope.registro.c2 = null;
                                $scope.registro.c3 = null;
                                $scope.registro.c4 = null;
                                $scope.registro.sin_criaderos = false;
                                $scope.registro.criaderos_eliminados = null;
                                $scope.registro.criaderos_tratados_fisicamente = null;
                                $scope.registro.criaderos_tratados_larvicida = null;
                                $scope.registro.larvicida_A = null;
                                $scope.registro.larvicida_B = null;
                                $scope.registro.larvicida_C = null;
                                $scope.registro.fecha = '';
                                $scope.registro.estado = '';
                                
                                $location.path('/registros/' + $scope.registro.id_S1);*/
                                
                            }, function(err) {
                                alert('Error Update RegistroS1: ' + JSON.stringify(err));
                            });

                        }
                        
                    }, function (err) {
                        alert( "error: " + JSON.stringify(err));
                        
                        /*$scope.registro.id = null;
                        $scope.registro.id_cA = null;
                        $scope.registro.id_cB = null;
                        $scope.registro.id_cC = null;
                        $scope.registro.manzana = '';
                        $scope.registro.direccion = '';
                        $scope.registro.latitude = null;
                        $scope.registro.longitude = null;
                        $scope.registro.precision = null;
                        $scope.registro.altitud = null;
                        $scope.registro.verificado = false;
                        $scope.registro.predio = null;
                        $scope.registro.jefe_familia = '';
                        $scope.registro.habitantes = null;
                        $scope.registro.febriles = null;
                        $scope.registro.a1 = null;
                        $scope.registro.a2 = null;
                        $scope.registro.a3 = null;
                        $scope.registro.a4 = null;
                        $scope.registro.a5 = null;
                        $scope.registro.a6 = null;
                        $scope.registro.a7 = null;
                        $scope.registro.a8 = null;
                        $scope.registro.a9 = null;
                        $scope.registro.a10 = null;
                        $scope.registro.b1 = null;
                        $scope.registro.b2 = null;
                        $scope.registro.b3 = null;
                        $scope.registro.b4 = null;
                        $scope.registro.b5 = null;
                        $scope.registro.b6 = null;
                        $scope.registro.c1 = null;
                        $scope.registro.c2 = null;
                        $scope.registro.c3 = null;
                        $scope.registro.c4 = null;
                        $scope.registro.sin_criaderos = false;
                        $scope.registro.criaderos_eliminados = null;
                        $scope.registro.criaderos_tratados_fisicamente = null;
                        $scope.registro.criaderos_tratados_larvicida = null;
                        $scope.registro.larvicida_A = null;
                        $scope.registro.larvicida_B = null;
                        $scope.registro.larvicida_C = null;
                        $scope.registro.fecha = '';
                        $scope.registro.estado = '';
                        
                        $location.path('/registros/' + $scope.registro.id_S1);*/
                    });

                });

            });

        };

        $scope.modificarRegistro = function(id) {

            $scope.registro.fecha = $filter('date')(new Date(), 'dd/MM/yyyy');

            db.transaction(function(tx) {

                /*var datos = [$scope.registro.manzana, $scope.registro.direccion, $scope.registro.verificado, $scope.registro.predio.id, $scope.registro.jefe_familia, $scope.registro.habitantes, $scope.registro.febriles, $scope.registro.sin_criaderos, $scope.registro.criaderos_eliminados, $scope.registro.criaderos_tratados_fisicamente, $scope.registro.criaderos_tratados_larvicida, $scope.registro.larvicida_A, $scope.registro.larvicida_B, $scope.registro.larvicida_C, $scope.registro.fecha, $scope.registro.estado, false, id];*/
                
                var datos = [$scope.registro.manzana, $scope.registro.direccion, $scope.registro.verificado, $scope.registro.predio.id, $scope.registro.jefe_familia, $scope.registro.habitantes, $scope.registro.febriles, $scope.registro.sin_criaderos, $scope.registro.criaderos_eliminados, $scope.registro.criaderos_tratados_fisicamente, $scope.registro.criaderos_tratados_larvicida, $scope.registro.larvicida_A, $scope.registro.fecha, $scope.registro.estado, false, id];

                /*var query = 'UPDATE RegistrosS1 SET manzana = ?, direccion = ?, verificado = ?, predio = ?, jefe_familia = ?, habitantes = ?, febriles = ?, sin_criaderos = ?, criaderos_eliminados = ?, criaderos_tratados_fisicamente = ?, criaderos_tratados_larvicida = ?, larvicida_A = ?, larvicida_B = ?, larvicida_C = ?, fecha = ?, estado = ?, enviado = ? WHERE id = ?';*/
                
                var query = 'UPDATE RegistrosS1 SET manzana = ?, direccion = ?, verificado = ?, predio = ?, jefe_familia = ?, habitantes = ?, febriles = ?, sin_criaderos = ?, criaderos_eliminados = ?, criaderos_tratados_fisicamente = ?, criaderos_tratados_larvicida = ?, larvicida_A = ?, fecha = ?, estado = ?, enviado = ? WHERE id = ?';

                tx.executeSql(query, datos, function(tx, res) {
                    
                    var datos = null;
                    var datos = [$scope.registro.a1, $scope.registro.a2, $scope.registro.a3, $scope.registro.a4, $scope.registro.a5, $scope.registro.a6, $scope.registro.a7, $scope.registro.a8, $scope.registro.a9, $scope.registro.a10, id];      

                    var query = 'UPDATE CriaderosA SET criaderos_A1 = ?, criaderos_A2 = ?, criaderos_A3 = ?, criaderos_A4 = ?, criaderos_A5 = ?, criaderos_A6 = ?, criaderos_A7 = ?, criaderos_A8 = ?, criaderos_A9 = ?, criaderos_A10 = ? WHERE id = ?';
                    tx.executeSql(query, datos, function(tx, res) {
                        //alert('UPDATE ID A-> ' + JSON.stringify(res));
                    });

                    var datos = null;

                    var datos = [$scope.registro.b1, $scope.registro.b2, $scope.registro.b3, $scope.registro.b4, $scope.registro.b5, $scope.registro.b6, id];

                    var query = 'UPDATE CriaderosB SET criaderos_B1 = ?, criaderos_B2 = ?, criaderos_B3 = ?, criaderos_B4 = ?, criaderos_B5 = ?, criaderos_B6 = ? WHERE id = ?';
                    tx.executeSql(query, datos, function(tx, res) {
                        //alert('UPDATE ID B-> ' + JSON.stringify(res));
                    });


                    var datos = null;
                    var datos = [$scope.registro.c1, $scope.registro.c2, $scope.registro.c3, $scope.registro.c4, id];

                    var query = 'UPDATE CriaderosC SET criaderos_C1 = ?, criaderos_C2 = ?, criaderos_C3 = ?, criaderos_C4 = ? WHERE id = ?';
                    tx.executeSql(query, datos, function(tx, res) {
                        //alert('UPDATE ID C-> ' + JSON.stringify(res));
                    });

                    var datos = null;
                    RestService.modificarRegistro($scope.registro).then(
                    function (response) {

                        if( response.data['respuesta'] == "OK"){
                            
                            var datos = [true, id];
                            var query = 'UPDATE RegistrosS1 SET enviado = ? WHERE id = ?';
                            $cordovaSQLite.execute(db, query, datos).then(
                            function(res) {
                                
                                /*$scope.registro.id = null;
                                $scope.registro.id_cA = null;
                                $scope.registro.id_cB = null;
                                $scope.registro.id_cC = null;
                                $scope.registro.manzana = '';
                                $scope.registro.direccion = '';
                                $scope.registro.latitude = null;
                                $scope.registro.longitude = null;
                                $scope.registro.precision = null;
                                $scope.registro.altitud = null;
                                $scope.registro.verificado = false;
                                $scope.registro.predio = null;
                                $scope.registro.jefe_familia = '';
                                $scope.registro.habitantes = null;
                                $scope.registro.febriles = null;
                                $scope.registro.a1 = null;
                                $scope.registro.a2 = null;
                                $scope.registro.a3 = null;
                                $scope.registro.a4 = null;
                                $scope.registro.a5 = null;
                                $scope.registro.a6 = null;
                                $scope.registro.a7 = null;
                                $scope.registro.a8 = null;
                                $scope.registro.a9 = null;
                                $scope.registro.a10 = null;
                                $scope.registro.b1 = null;
                                $scope.registro.b2 = null;
                                $scope.registro.b3 = null;
                                $scope.registro.b4 = null;
                                $scope.registro.b5 = null;
                                $scope.registro.b6 = null;
                                $scope.registro.c1 = null;
                                $scope.registro.c2 = null;
                                $scope.registro.c3 = null;
                                $scope.registro.c4 = null;
                                $scope.registro.sin_criaderos = false;
                                $scope.registro.criaderos_eliminados = null;
                                $scope.registro.criaderos_tratados_fisicamente = null;
                                $scope.registro.criaderos_tratados_larvicida = null;
                                $scope.registro.larvicida_A = null;
                                $scope.registro.larvicida_B = null;
                                $scope.registro.larvicida_C = null;
                                $scope.registro.fecha = '';
                                $scope.registro.estado = '';
                                
                                $location.path('/registros/' + $scope.registro.id_S1);*/
                                
                            }, function(err) {
                                alert('Error Update RegistroS1: ' + JSON.stringify(err));
                            });

                        }

                    }, function (err) {
                        alert( "error: " + JSON.stringify(err));
                   
                        /*$scope.registro.id = null;
                        $scope.registro.id_cA = null;
                        $scope.registro.id_cB = null;
                        $scope.registro.id_cC = null;
                        $scope.registro.manzana = '';
                        $scope.registro.direccion = '';
                        $scope.registro.latitude = null;
                        $scope.registro.longitude = null;
                        $scope.registro.precision = null;
                        $scope.registro.altitud = null;
                        $scope.registro.verificado = false;
                        $scope.registro.predio = null;
                        $scope.registro.jefe_familia = '';
                        $scope.registro.habitantes = null;
                        $scope.registro.febriles = null;
                        $scope.registro.a1 = null;
                        $scope.registro.a2 = null;
                        $scope.registro.a3 = null;
                        $scope.registro.a4 = null;
                        $scope.registro.a5 = null;
                        $scope.registro.a6 = null;
                        $scope.registro.a7 = null;
                        $scope.registro.a8 = null;
                        $scope.registro.a9 = null;
                        $scope.registro.a10 = null;
                        $scope.registro.b1 = null;
                        $scope.registro.b2 = null;
                        $scope.registro.b3 = null;
                        $scope.registro.b4 = null;
                        $scope.registro.b5 = null;
                        $scope.registro.b6 = null;
                        $scope.registro.c1 = null;
                        $scope.registro.c2 = null;
                        $scope.registro.c3 = null;
                        $scope.registro.c4 = null;
                        $scope.registro.sin_criaderos = false;
                        $scope.registro.criaderos_eliminados = null;
                        $scope.registro.criaderos_tratados_fisicamente = null;
                        $scope.registro.criaderos_tratados_larvicida = null;
                        $scope.registro.larvicida_A = null;
                        $scope.registro.larvicida_B = null;
                        $scope.registro.larvicida_C = null;
                        $scope.registro.fecha = '';
                        $scope.registro.estado = '';
                        
                        $location.path('/registros/' + $scope.registro.id_S1);*/
                        
                    });

                });

            });

        };

        $scope.showGuardando = function() {
            $ionicLoading.show({
                template: 'Guardando...'
            });
        };

        $scope.hideGuardando = function() {
            $ionicLoading.hide();
        };
        
        $scope.inicializarRegistro = function() {
            $scope.registro.id = null;
            $scope.registro.id_server = null;
            $scope.registro.id_cA = null;
            $scope.registro.id_cB = null;
            $scope.registro.id_cC = null;
            $scope.registro.manzana = '';
            $scope.registro.direccion = '';
            $scope.registro.latitude = null;
            $scope.registro.longitude = null;
            $scope.registro.precision = null;
            $scope.registro.altitud = null;
            $scope.registro.verificado = false;
            $scope.registro.predio = null;
            $scope.registro.jefe_familia = '';
            $scope.registro.habitantes = null;
            $scope.registro.febriles = null;
            $scope.registro.a1 = null;
            $scope.registro.a2 = null;
            $scope.registro.a3 = null;
            $scope.registro.a4 = null;
            $scope.registro.a5 = null;
            $scope.registro.a6 = null;
            $scope.registro.a7 = null;
            $scope.registro.a8 = null;
            $scope.registro.a9 = null;
            $scope.registro.a10 = null;
            $scope.registro.b1 = null;
            $scope.registro.b2 = null;
            $scope.registro.b3 = null;
            $scope.registro.b4 = null;
            $scope.registro.b5 = null;
            $scope.registro.b6 = null;
            $scope.registro.c1 = null;
            $scope.registro.c2 = null;
            $scope.registro.c3 = null;
            $scope.registro.c4 = null;
            $scope.registro.sin_criaderos = false;
            $scope.registro.criaderos_eliminados = null;
            $scope.registro.criaderos_tratados_fisicamente = null;
            $scope.registro.criaderos_tratados_larvicida = null;
            $scope.registro.larvicida_A = null;
            //$scope.registro.larvicida_B = null;
            //$scope.registro.larvicida_C = null;
            $scope.registro.fecha = '';
            $scope.registro.estado = '';    
        };
        
        $scope.guardar = function() {
            
            if (window.cordova) {
                cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
                    //alert("Location is " + (enabled ? "enabled" : "disabled"));
                    if(enabled == false){
                        alert("Active el GPS");
                        $scope.sw = false;
                    }
                }, function(error) {
                    alert("Error al verificar GPS: " + error);
                });
            }; 
            
            $scope.validarDatos();
            if($scope.sw === true){
                
                if (window.cordova) {
                    cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
                        //alert("Location is " + (enabled ? "enabled" : "disabled"));
                        if(enabled == false){
                            alert("Active el GPS");
                            //$scope.sw = false;
                        }
                        else if ($scope.registro.id == null) {

                            var fecha = $filter('date')(new Date(), 'dd/MM/yyyy');
                            var datos = [$scope.registro.manzana, $scope.registro.direccion, $scope.registro.predio.id, fecha, "ACTIVO"];

                            var query = 'SELECT * FROM RegistrosS1 WHERE manzana = ? and direccion = ? and predio = ? and fecha = ? and estado = ?';

                            $cordovaSQLite.execute(db, query, datos).then(
                            function(res) {
                                    if(res.rows.length <= 0){
                                        $scope.showGuardando();
                                        $scope.obtenerCoordenadas();
                                        setTimeout(function() {
                                            $scope.insertarRegistro();
                                            $scope.hideGuardando();
                                            $scope.showAlert();
                                        }, 10000);                                    
                                }else{
                                    alert("La planilla ya existe");    
                                }

                            }, function(err) {
                                alert('Error verificar Insertar RegistroS1: ' + err);
                            });

                        } else {
                            $scope.showGuardando();
                            $scope.modificarRegistro($scope.registro.id);               
                            $scope.hideGuardando();
                            $scope.showAlert();
                        }    
                        
                    }, function(error) {
                        alert("Error al verificar GPS: " + error);
                    });
                }; 
                
                
                
                //
                /*if ($scope.registro.id == null) {
                    
                    var fecha = $filter('date')(new Date(), 'dd/MM/yyyy');
                    var datos = [$scope.registro.manzana, $scope.registro.direccion, $scope.registro.predio.id, fecha, "ACTIVO"];

                    var query = 'SELECT * FROM RegistrosS1 WHERE manzana = ? and direccion = ? and predio = ? and fecha = ? and estado = ?';

                    $cordovaSQLite.execute(db, query, datos).then(
                    function(res) {
                            if(res.rows.length <= 0){
                                $scope.showGuardando();
                                $scope.obtenerCoordenadas();
                                setTimeout(function() {
                                    $scope.insertarRegistro();
                                    $scope.hideGuardando();
                                    $scope.showAlert();
                                }, 10000);                                    
                        }else{
                            alert("La planilla ya existe");    
                        }

                    }, function(err) {
                        alert('Error verificar Insertar RegistroS1: ' + err);
                    });

                } else {
                    $scope.showGuardando();
                    $scope.modificarRegistro($scope.registro.id);               
                    $scope.hideGuardando();
                    $scope.showAlert();
                }*/
                //
            
            }
        };
        
        $scope.validarDatos = function() {

            $scope.sw = true;
            
            if($scope.registro.manzana === ''){
                alert("Ingrese el código de Manzana");
                $scope.sw = false;

            }else if($scope.registro.direccion === ''){
                alert("Ingrese la Dirección");
                $scope.sw = false;
                
            }
            else if($scope.registro.predio === null){
                alert("Ingrese el Tipo de Predio");
                $scope.sw = false;
                
            }
        };

    }, false);
    
    $ionicPlatform.registerBackButtonAction(function() {
        //var path = $location.path()
        //alert("state: " + $state.current.name);
        if ($state.current.name === "done") {
            $scope.showConfirm();
            //$ionicHistory.goBack();
        }
    }, 100);

})
