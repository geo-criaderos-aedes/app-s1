var db = null;
angular.module('eventsApp', ['ionic',
    'eventsApp.controllers',
    'eventsApp.services'
])

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    $ionicConfigProvider.views.maxCache(0);
    $ionicConfigProvider.backButton.previousTitleText(false);
    $ionicConfigProvider.backButton.text('');

    $stateProvider.state('tab', {
            url: "/tab",
            abstract: true,
            templateUrl: "templates/tabs.html"
        })
        .state('tab.list', {
            url: '/list',
            views: {
                'tab-list': {
                    templateUrl: 'templates/list.html',
                    controller: 'ListCtrl'
                }
            }
        })
        .state('tab.operaciones', {
            url: '/operaciones',
            views: {
                'tab-operaciones': {
                    templateUrl: 'templates/operaciones.html',
                    controller: 'OperacionesCtrl'
                }
            }
        })
        .state('tab.account', {
            url: '/account',
            views: {
                'tab-account': {
                    templateUrl: 'templates/account.html',
                    controller: 'AccountCtrl'
                }
            }
        })
        .state('login', {
            url: '/login',
            templateUrl: 'templates/login.html',
            controller: 'LoginCtrl'
        })
        .state('configuraciones', {
            url: "/configuraciones",
            templateUrl: "templates/configuraciones.html",
            controller: 'ConfiguracionesCtrl'
        })
        .state('cambiar-contrasena', {
            url: '/cambiar-contrasena',
            templateUrl: 'templates/cambiar-contrasena.html',
            controller: 'CambiarContrasenaCtrl'
        })
        .state('nuevo-usuario', {
            url: '/nuevo-usuario',
            templateUrl: 'templates/nuevo-usuario.html',
            controller: 'NuevoUsuarioCtrl'
        })
        .state('nueva-planilla', {
            url: '/nueva-planilla/{id}',
            templateUrl: 'templates/nueva-planilla.html',
            controller: 'NuevaPlanillaCtrl'
        })
        .state('registros', {
            url: '/registros/{id}',
            templateUrl: 'templates/registros.html',
            controller: 'RegistrosCtrl'
        })
        .state('step1', {
            url: "/step1",
            data: {
                step: 1
            },
            templateUrl: "templates/step1.html",
            controller: 'Step1Ctrl'
        })
        .state('step2', {
            url: "/step2",
            data: {
                step: 2
            },
            templateUrl: "templates/step2.html",
            controller: 'Step2Ctrl'
        })
        .state('step3', {
            url: "/step3",
            data: {
                step: 3
            },
            templateUrl: "templates/step3.html",
            controller: 'Step3Ctrl'
        })
        .state('step4', {
            url: "/step4",
            data: {
                step: 4
            },
            templateUrl: "templates/step4.html",
            controller: 'Step4Ctrl'
        })
        .state('step5', {
            url: "/step5",
            data: {
                step: 5
            },
            templateUrl: "templates/step5.html",
            controller: 'Step5Ctrl'
        })
        .state('done', {
            url: "/done",
            data: {
                step: 6
            },
            templateUrl: "templates/done.html",
            controller: 'DoneCtrl'
        })

    $urlRouterProvider.otherwise('/login');
})

.run(function($ionicPlatform, $cordovaSQLite, $filter) {
    document.addEventListener('deviceready', function() {
        $ionicPlatform.ready(function() {
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }

            //se desactiva el boton atras
            $ionicPlatform.registerBackButtonAction(function(event) {
                event.preventDefault();
            }, 100);

            /*window.sqlitePlugin.deleteDatabase({name: 'my.db', location: 'default'}, function() {
                //alert('Successfully deleted database');
            }, function() {
                alert('Error while delete database');
            });*/
            
            /*Intent intent = getIntent();
            Uri data = intent.getData();*/
            //String path = data.getPath();


            var db = window.sqlitePlugin.openDatabase({
                name: 'my.db',
                location: 'default'
            });
            
            /*$cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS TipoControl');
            $cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS TiposPredio');
            $cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS Usuarios');
            $cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS Departamentos');
            $cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS Distritos');
            $cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS Barrios');
            
            
            $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS TipoControl (id integer primary key, nombre text)');

            $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS TiposPredio (id integer primary key, nombre text)');

            //$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS Usuarios (id integer primary key, cedula text, nombre text, apellido text, password text, fecha_nacimiento date, pregunta text, respuesta text, fecha_creacion date, estado text)');
            
            $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS Usuarios (id integer primary key, cedula text, nombre text, apellido text)')

            $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS Departamentos (id integer primary key, nombre text)');

            $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS Distritos (id integer primary key, id_departamento integer, nombre text, FOREIGN KEY(id_departamento) REFERENCES Departamentos(id))');

            $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS Barrios (id integer primary key, id_distrito integer, nombre text, FOREIGN KEY(id_distrito) REFERENCES Distritos(id))');*/

            $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS S1 (id integer primary key autoincrement, id_server integer, tipo_control integer, departamento integer, distrito integer, barrio integer, encargado integer, supervisor integer, operador integer, cuadrilla text, brigada text, usuario integer, fecha date, estado text, enviado boolean, FOREIGN KEY(tipo_control) REFERENCES TipoControl(id), FOREIGN KEY(departamento) REFERENCES Departamento(id), FOREIGN KEY(distrito) REFERENCES Distritos(id), FOREIGN KEY(barrio) REFERENCES Barrios(id), FOREIGN KEY(encargado) REFERENCES Usuarios(id), FOREIGN KEY(supervisor) REFERENCES Usuarios(id), FOREIGN KEY(operador) REFERENCES Usuarios(id), FOREIGN KEY(usuario) REFERENCES Usuarios(id))');

            $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS RegistrosS1 (id integer primary key autoincrement, id_server integer, id_S1 integer, manzana text, direccion text, latitude text, longitude text, precision text, altitud text, verificado boolean, predio integer, jefe_familia text, habitantes integer, febriles integer, sin_criaderos boolean, criaderos_eliminados integer, criaderos_tratados_fisicamente integer, criaderos_tratados_larvicida integer, larvicida_A integer, larvicida_B integer, larvicida_C integer, fecha date, estado text, enviado boolean, FOREIGN KEY(id_S1) REFERENCES S1(id), FOREIGN KEY(predio) REFERENCES TiposPredio(id))');

            $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS CriaderosA (id integer primary key autoincrement, id_registro_S1 integer, criaderos_A1 integer, criaderos_A2 integer, criaderos_A3 integer, criaderos_A4 integer, criaderos_A5 integer, criaderos_A6 integer, criaderos_A7 integer, criaderos_A8 integer, criaderos_A9 integer, criaderos_A10 integer, FOREIGN KEY(id_registro_S1) REFERENCES RegistrosS1(id))');

            $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS CriaderosB (id integer primary key autoincrement, id_registro_S1 integer, criaderos_B1 integer, criaderos_B2 integer, criaderos_B3 integer, criaderos_B4 integer, criaderos_B5 integer, criaderos_B6 integer, FOREIGN KEY(id_registro_S1) REFERENCES RegistrosS1(id))');

            $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS CriaderosC (id integer primary key autoincrement, id_registro_S1 integer, criaderos_C1 integer, criaderos_C2 integer, criaderos_C3 integer, criaderos_C4 integer)');
            
            //$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS Configuraciones (id integer primary key autoincrement, path text)');
            
            var fecha = $filter('date')(new Date(), 'dd/MM/yyyy');
            var datos = [fecha, true];
            var query = 'DELETE FROM CriaderosA WHERE id IN (SELECT id FROM RegistrosS1 WHERE fecha < ? and enviado = ?)';
            $cordovaSQLite.execute(db, query, datos).then(
            function(res) {
                //alert('criaderosA eliminados: ' + JSON.stringify(res));
            }, function(err) {
                alert('Error barrido CriaderosA: ' + JSON.stringify(err));
            });
                    
            var query = 'DELETE FROM CriaderosB WHERE id IN (SELECT id FROM RegistrosS1 WHERE fecha < ? and enviado = ?)';
            $cordovaSQLite.execute(db, query, datos).then(
            function(res) {
                //alert('criaderosB eliminados : ' + JSON.stringify(res));
            }, function(err) {
                alert('Error barrido CriaderosB: ' + JSON.stringify(err));
            });
                    
            var query = 'DELETE FROM CriaderosC WHERE id IN (SELECT id FROM RegistrosS1 WHERE fecha < ? and enviado = ?)';
            $cordovaSQLite.execute(db, query, datos).then(
            function(res) {
                //alert('criaderosC eliminados: ' + JSON.stringify(res));
            }, function(err) {
                alert('Error barrido CriaderosC: ' + JSON.stringify(err));
            });
                    
            var query = 'DELETE FROM RegistrosS1 WHERE fecha < ? and enviado = ?';
            $cordovaSQLite.execute(db, query, datos).then(
            function(res) {
                //alert('registros eliminados  : ' + JSON.stringify(res));
            }, function(err) {
                alert('Error barrido RegistrosS1: ' + JSON.stringify(err));
            });
                    
            var query = 'DELETE FROM S1 WHERE fecha < ? and enviado = ? and 0 = (SELECT COUNT(*) FROM RegistrosS1 WHERE id_S1 = S1.id)';
            $cordovaSQLite.execute(db, query, datos).then(
            function(res) {
                //alert('planilla eliminadas  : ' + JSON.stringify(res));
            }, function(err) {
                alert('Error barrido S1: ' + JSON.stringify(err));
            });
            
            /*var query = 'INSERT INTO Configuraciones (path) VALUES (?)';
            $cordovaSQLite.execute(db, query, [
                'http://192.168.43.227:8080'
            ]).then(function(res) {
                alert('INSERT ID -> ' + res.insertId);
            }, function(err) {
                alert('Error Configuraciones: ' + err);
            });*/

            /*var query = 'INSERT INTO TipoControl (nombre) VALUES (?),(?),(?)';

            $cordovaSQLite.execute(db, query, [
                ['rastrillaje'],
                ['minga'],
                ['bloqueo']
            ]).then(function(res) {
                //alert('INSERT ID -> ' + res.insertId);
            }, function(err) {
                alert('Error TipoControl: ' + err);
            });*/

            /*var query = 'INSERT INTO TiposPredio (nombre) VALUES (?),(?),(?),(?),(?),(?),(?),(?),(?),(?),(?),(?),(?),(?)';

            $cordovaSQLite.execute(db, query, [
                ['vivienda'],
                ['baldio'],
                ['comercio'],
                ['gomeria'],
                ['taller'],
                ['chatarreria'],
                ['recicladora'],
                ['construccion'],
                ['cerrado'],
                ['renuente'],
                ['deshabitada'],
                ['inst. publica'],
                ['hospital/sanatorio'],
                ['otros'],
            ]).then(function(res) {
                alert('INSERT ID -> ' + res.insertId);
            }, function(err) {
                alert('Error TiposPredio: ' + err);
            });

            /*var query = 'INSERT INTO Usuarios (cedula, nombre, apellido,  password, fecha_nacimiento, pregunta, respuesta, fecha_creacion, estado) VALUES (?,?,?,?,?,?,?,?,?)';

            $cordovaSQLite.execute(db, query, ['4000000', 'juan', 'perez', 'admin', '01/01/2000', 'admin', 'admin', '01/01/2000', 'i']).then(function(res) {
                //alert('INSERT ID -> ' + res.insertId);
            }, function(err) {
                alert('Error Usuarios: ' + err);
            });

            var query = 'INSERT INTO Usuarios (cedula, nombre, apellido,  password, fecha_nacimiento, pregunta, respuesta, fecha_creacion, estado) VALUES (?,?,?,?,?,?,?,?,?)';

            $cordovaSQLite.execute(db, query, ['4118015', 'angel', 'mendoza', 'admin', '19/06/86', 'admin', 'admin', '19/06/1986', 'a']).then(function(res) {
                //alert('INSERT ID -> ' + res.insertId);
            }, function(err) {
                alert('Error Usuarios: ' + err);
            });*/

            /*var query = 'INSERT INTO Departamentos (nombre) VALUES (?)';

            $cordovaSQLite.execute(db, query, [
                'central'
            ]).then(function(res) {
                //alert('INSERT ID -> ' + res.insertId);
            }, function(err) {
                alert('Error Departamentos: ' + err);
            });*/

            /*var query = 'INSERT INTO Distritos (id_departamento, nombre) VALUES (?,?)'

            $cordovaSQLite.execute(db, query, [1, 'asuncion']).then(function(res) {
                //alert('INSERT ID -> ' + res.insertId);
            }, function(err) {
                alert('Error Distritos: ' + err);
            });

            var query = 'INSERT INTO Distritos (id_departamento, nombre) VALUES (?,?)'

            $cordovaSQLite.execute(db, query, [1, 'san lorenzo']).then(function(res) {
                //alert('INSERT ID -> ' + res.insertId);
            }, function(err) {
                alert('Error Distritos: ' + err);
            });*/

            /*var query = 'INSERT INTO Barrios (id_distrito, nombre) VALUES (?,?)'

            $cordovaSQLite.execute(db, query, [1, 'pozo favorito']).then(function(res) {
                //alert('INSERT ID -> ' + res.insertId);
            }, function(err) {
                alert('Error Barrios: ' + err);
            });

            var query = 'INSERT INTO Barrios (id_distrito, nombre) VALUES (?,?)'

            $cordovaSQLite.execute(db, query, [2, 'santa lucia']).then(function(res) {
                //alert('INSERT ID -> ' + res.insertId);
            }, function(err) {
                alert('Error Barrios: ' + err);
            });*/

            /*var query = 'INSERT INTO S1 (id_server, tipo_control, departamento, distrito,  barrio, encargado, supervisor, operador, cuadrilla, brigada, usuario, fecha, estado, enviado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

            $cordovaSQLite.execute(db, query, [null, 1, 1, 1, 1, 1, 1, 1, '01', '01', 1, '08/10/2016', 'ACTIVO', false]).then(
            function(res) {
                alert('INSERT ID -> ' + res.insertId);
            }, function (err) {
                alert('Error S1: ' + JSON.stringify(err));
            });
            
           var query = 'INSERT INTO S1 (id_server, tipo_control, departamento, distrito,  barrio, encargado, supervisor, operador, cuadrilla, brigada, usuario, fecha, estado, enviado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

            $cordovaSQLite.execute(db, query, [null, 1, 1, 1, 2, 1, 1, 1, '02', '02', 1, '08/10/2016', 'ACTIVO', false]).then(
            function(res) {
                alert('INSERT ID -> ' + res.insertId);
            }, function (err) {
                alert('Error S1: ' + JSON.stringify(err));
            });
            
           var query = 'INSERT INTO S1 (id_server, tipo_control, departamento, distrito,  barrio, encargado, supervisor, operador, cuadrilla, brigada, usuario, fecha, estado, enviado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

            $cordovaSQLite.execute(db, query, [null, 2, 1, 2, 2, 1, 1, 1, '03', '03', 1, '08/10/2016', 'ACTIVO', false]).then(
            function(res) {
                alert('INSERT ID -> ' + res.insertId);
            }, function (err) {
                alert('Error S1: ' + JSON.stringify(err));
            });
            
            var query = 'INSERT INTO S1 (id_server, tipo_control, departamento, distrito,  barrio, encargado, supervisor, operador, cuadrilla, brigada, usuario, fecha, estado, enviado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

            $cordovaSQLite.execute(db, query, [null, 2, 2, 2, 2, 1, 1, 1, '04', '04', 1, '08/10/2016', 'ACTIVO', false]).then(
            function(res) {
                alert('INSERT ID -> ' + res.insertId);
            }, function (err) {
                alert('Error S1: ' + JSON.stringify(err));
            });

            
            var query = 'INSERT INTO S1 (id_server, tipo_control, departamento, distrito,  barrio, encargado, supervisor, operador, cuadrilla, brigada, usuario, fecha, estado, enviado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

            $cordovaSQLite.execute(db, query, [null, 3, 1, 2, 2, 1, 1, 1, '05', '05', 1, '08/10/2016', 'ACTIVO', false]).then(
            function(res) {
                alert('INSERT ID -> ' + res.insertId);
            }, function (err) {
                alert('Error S1: ' + JSON.stringify(err));
            });
            
            var query = 'INSERT INTO S1 (id_server, tipo_control, departamento, distrito,  barrio, encargado, supervisor, operador, cuadrilla, brigada, usuario, fecha, estado, enviado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

            $cordovaSQLite.execute(db, query, [null, 3, 2, 2, 2, 1, 1, 1, '06', '06', 1, '08/10/2016', 'ACTIVO', false]).then(
            function(res) {
                alert('INSERT ID -> ' + res.insertId);
            }, function (err) {
                alert('Error S1: ' + JSON.stringify(err));
            });
            
            var query = 'INSERT INTO S1 (id_server, tipo_control, departamento, distrito,  barrio, encargado, supervisor, operador, cuadrilla, brigada, usuario, fecha, estado, enviado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

            $cordovaSQLite.execute(db, query, [null, 3, 3, 2, 2, 1, 1, 1, '07', '07', 1, '08/10/2016', 'ACTIVO', false]).then(
            function(res) {
                alert('INSERT ID -> ' + res.insertId);
            }, function (err) {
                alert('Error S1: ' + JSON.stringify(err));
            });*/


            /*var query = 'INSERT INTO RegistrosS1 (id_server, id_S1, manzana, direccion, latitude, longitude, precision, altitud, verificado, predio, jefe_familia, habitantes, febriles, sin_criaderos, criaderos_eliminados, criaderos_tratados_fisicamente, criaderos_tratados_larvicida, larvicida_A, larvicida_B, larvicida_C, fecha, estado, enviado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

            $cordovaSQLite.execute(db, query, [ null, 1, '1', 'amistad 121', '1', '1', '1', '1', true, 1, 'juan mendoza', 3, 1, false, 1, 1, 0, 0, 0, 0, '08/10/2016', 'ACTIVO', false]).
            then(function(res) {
                alert('INSERT ID -> ' + res.insertId);
            }, function (err) {
                alert('Error RegistrosS1: ' + err);
            });
            
            var query = 'INSERT INTO RegistrosS1 (id_server, id_S1, manzana, direccion, latitude, longitude, precision, altitud, verificado, predio, jefe_familia, habitantes, febriles, sin_criaderos, criaderos_eliminados, criaderos_tratados_fisicamente, criaderos_tratados_larvicida, larvicida_A, larvicida_B, larvicida_C, fecha, estado, enviado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

            $cordovaSQLite.execute(db, query, [ null, 1, '2', 'amistad 122', '1', '1', '1', '1', true, 1, 'juan mendoza', 3, 1, false, 1, 1, 0, 0, 0, 0, '08/10/2016', 'ACTIVO', false]).
            then(function(res) {
                alert('INSERT ID -> ' + res.insertId);
            }, function (err) {
                alert('Error RegistrosS1: ' + err);
            });
            
            var query = 'INSERT INTO RegistrosS1 (id_server, id_S1, manzana, direccion, latitude, longitude, precision, altitud, verificado, predio, jefe_familia, habitantes, febriles, sin_criaderos, criaderos_eliminados, criaderos_tratados_fisicamente, criaderos_tratados_larvicida, larvicida_A, larvicida_B, larvicida_C, fecha, estado, enviado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

            $cordovaSQLite.execute(db, query, [ null, 1, '3', 'amistad 123', '1', '1', '1', '1', true, 1, 'juan mendoza', 3, 1, false, 1, 1, 0, 0, 0, 0, '08/10/2016', 'ACTIVO', false]).
            then(function(res) {
                alert('INSERT ID -> ' + res.insertId);
            }, function (err) {
                alert('Error RegistrosS1: ' + err);
            });
            
            var query = 'INSERT INTO RegistrosS1 (id_server, id_S1, manzana, direccion, latitude, longitude, precision, altitud, verificado, predio, jefe_familia, habitantes, febriles, sin_criaderos, criaderos_eliminados, criaderos_tratados_fisicamente, criaderos_tratados_larvicida, larvicida_A, larvicida_B, larvicida_C, fecha, estado, enviado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

            $cordovaSQLite.execute(db, query, [ null, 1, '4', 'amistad 123', '1', '1', '1', '1', true, 1, 'juan mendoza', 3, 1, false, 1, 1, 0, 0, 0, 0, '08/10/2016', 'ACTIVO', false]).
            then(function(res) {
                alert('INSERT ID -> ' + res.insertId);
            }, function (err) {
                alert('Error RegistrosS1: ' + err);
            });

            var query = 'INSERT INTO RegistrosS1 (id_server, id_S1, manzana, direccion, latitude, longitude, precision, altitud, verificado, predio, jefe_familia, habitantes, febriles, sin_criaderos, criaderos_eliminados, criaderos_tratados_fisicamente, criaderos_tratados_larvicida, larvicida_A, larvicida_B, larvicida_C, fecha, estado, enviado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

            $cordovaSQLite.execute(db, query, [ null, 1, '5', 'amistad 123', '1', '1', '1', '1', true, 1, 'juan mendoza', 3, 1, false, 1, 1, 0, 0, 0, 0, '08/10/2016', 'ACTIVO', false]).
            then(function(res) {
                alert('INSERT ID -> ' + res.insertId);
            }, function (err) {
                alert('Error RegistrosS1: ' + err);
            });*/

        });
    }, false);
});
