angular.module('eventsApp.services', [])

.factory('EventsService', function($http) {
    return {
        all: function() {

            // TODO: get events from backend
            var events = [{
                'speaker': 'Juan Perez',
                'title': 'PlanillaS1-21-04-16',
                'place': 'Ciudad del Este',
                'date': '15:00'
            }, {
                'speaker': 'Arnaldo Mendez',
                'title': 'PlanillaS1-21-04-16',
                'place': 'San Lorenzo',
                'date': '17:00'
            }, {
                'speaker': 'Ignacio Sanchez',
                'title': 'PlanillaS1-21-04-16',
                'place': 'Asunción',
                'date': '19:00'
            }];
            return events;
        }
    };
})

.factory('RecordsService', function($http) {
    return {
        all: function() {

            // TODO: get events from backend
            var records = [{
                'speaker': 'PlanillaS1-21-04-16',
                'title': 'Registro-01',
                'date': '15:00'
            }, {
                'speaker': 'PlanillaS1-21-04-16',
                'title': 'Registro-02',
                'date': '17:00'
            }, {
                'speaker': 'PlanillaS1-21-04-16',
                'title': 'Registro-03',
                'date': '19:00'
            }];
            return records;
        }
    };
})

.factory('RestService', function($http, $location, $rootScope) {
    //var ip_servidor = $rootScope.ip_servidor;
    //var ip_servidor = 'http://104.236.180.211:8088';
    //var ip_servidor = 'http://192.168.43.227:8080';
    var ip_servidor = 'http://192.168.1.8:8080';
    //var ip_servidor = 'http://10.133.21.185:8080';
    
    return {
        postAuthorization: function (username, password) {
            
            //var url = "http://192.168.43.227:8080/movil/login";
            //alert("rootScope: " + $rootScope.ip_servidor);
            var url = ip_servidor + "/movil/login";
            //var url = $rootScope.ip_servidor + "/movil/login";
            //alert("url: " + url);
            var data = JSON.stringify({
                username: username,
                password: password
            });
            
            $http.defaults.headers.post["Content-Type"] = "application/json";
            return $http.post(url, data, {}).then(
                function(success){
                    /*if (success.data['respuesta'] == true)
                        $location.path('/tab/operaciones');
                    else                        
                        alert("Login Incorrecto");*/
                    return success
                }, function(error){
                    alert('error: ' + error);   
                });
            
        },
        
        postCrearUsuario: function (user) {
            
            //$scope.respuesta = null;
            //var url = $rootScope.ip_servidor + "/movil/usuarios/alta";
            var url = ip_servidor + "/movil/usuarios/alta";
            var data = JSON.stringify({
                id_usuario: user.id_usuario,              
                contrasena: user.contrasena,
                contrasena2: user.contrasena2,
                nombre: user.nombre,
                apellido: user.apellido,
                fecha_nacimiento: user.fecha_nacimiento
            });
            
            $http.defaults.headers.post["Content-Type"] = "application/json";
            return $http.post(url, data, {}).then(
                function(success){
                    return success;
                }, function(error){
                    return error;
                });
            
        },
        
        listarDepartamentos: function () {
            
            //$scope.respuesta = null;
            //var url = $rootScope.ip_servidor + "/movil/departamentos";
            var url = ip_servidor + "/movil/departamentos";
            var data = null;
            
            //$http.defaults.headers.post["Content-Type"] = "application/json";
            return $http.get(url, data, {}).then(
                function(success){
                    return success;
                }, function(error){
                    return error;
                });
            
        },
        
        listarDistritos: function (departamento) {
            
            //$scope.respuesta = null;
            //var url = $rootScope.ip_servidor + "/movil/distritos/" + departamento;
            var url = ip_servidor + "/movil/distritos/" + departamento;
            var data = null;
            
            //$http.defaults.headers.post["Content-Type"] = "application/json";
            return $http.get(url, data, {}).then(
                function(success){
                    return success;
                }, function(error){
                    return error;
                });
            
        },
        
        listarBarrios: function (distrito) {
            
            //$scope.respuesta = null;
            //var url = $rootScope.ip_servidor + "/movil/barrios/" + distrito;
            var url = ip_servidor + "/movil/barrios/" + distrito;
            var data = null;
            
            //$http.defaults.headers.post["Content-Type"] = "application/json";
            return $http.get(url, data, {}).then(
                function(success){
                    return success;
                }, function(error){
                    return error;
                });
            
        },
        
        listarTiposPredios: function () {
            
            //$scope.respuesta = null;
            //var url = $rootScope.ip_servidor + "/movil/tipospredios";
            var url = ip_servidor + "/movil/tipospredios";
            var data = null;
            
            //$http.defaults.headers.post["Content-Type"] = "application/json";
            return $http.get(url, data, {}).then(
                function(success){
                    return success;
                }, function(error){
                    return error;
                });
            
        },
        
        listarTiposControles: function () {
            
            //$scope.respuesta = null;
            //var url = $rootScope.ip_servidor + "/movil/tiposcontroles";
            var url = ip_servidor + "/movil/tiposcontroles";
            var data = null;
            
            //$http.defaults.headers.post["Content-Type"] = "application/json";
            return $http.get(url, data, {}).then(
                function(success){
                    return success;
                }, function(error){
                    return error;
                });
            
        },
        
        crearPlanilla: function (planilla) {
            
            //$scope.respuesta = null;
            //var url = $rootScope.ip_servidor + "/movil/tablas1/alta";
            var url = ip_servidor + "/movil/tablas1/alta";
            var data = JSON.stringify({             
                fk_tipo_control: planilla.tipo_control.id,
                fk_barrio: planilla.barrio.id,
                //fk_distrito: planilla.distrito.id,
                //fk_departamento: planilla.barrio.id,
                fk_encargado: planilla.encargado.id,
                fk_supervisor: planilla.supervisor.id,
                fk_operador: planilla.operador.id,
                cuadrilla: planilla.cuadrilla,
                brigada: planilla.brigada,
                fk_usuario: planilla.usuario
            });
            $http.defaults.headers.post["Content-Type"] = "application/json";
            return $http.post(url, data, {}).then(
                function(success){
                    return success;
                }, function(error){
                    return error;
                });
            
        },
        
        listarUsuarios: function () {
            
            //$scope.respuesta = null;
            //var url = $rootScope.ip_servidor + "/movil/usuarios";
            var url = ip_servidor + "/movil/usuarios";
            var data = null;
            
            //$http.defaults.headers.post["Content-Type"] = "application/json";
            return $http.get(url, data, {}).then(
                function(success){
                    return success;
                }, function(error){
                    return error;
                });
            
        },
        
        crearRegistro: function (registro) {         
            //$scope.respuesta = null;
            //var url = $rootScope.ip_servidor + "/movil/registro/alta";
            var url = ip_servidor + "/movil/registro/alta";
            var data = JSON.stringify({           
                fk_tabla_s1: registro.id_S1_server,
                manzana: registro.manzana,
                direccion: registro.direccion,
                latitud: registro.latitude,
                longitud: registro.longitude,
                //precision: registro.precision,
                //altitud: registro.altitud,
                verificado: registro.verificado,
                fk_tipo_predio: registro.predio.id,
                jefe_familia: registro.jefe_familia,
                habitantes: registro.habitantes,
                febriles: registro.febriles,
                sin_criaderos: registro.sin_criaderos,
                criaderos_eliminados: registro.criaderos_eliminados,
                tratados_fisicamente: registro.criaderos_tratados_fisicamente,
                tratados_larvicida: registro.criaderos_tratados_larvicida,
                larvicida_a: registro.larvicida_A,
                //larvicida_b: registro.larvicida_B,
                //larvicida_c: registro.larvicida_C,
                //fk_registro_s1_a: registro.id_cA,
                criaderos_a1: registro.a1,
                criaderos_a2: registro.a2,
                criaderos_a3: registro.a3,
                criaderos_a4: registro.a4,
                criaderos_a5: registro.a5,
                criaderos_a6: registro.a6,
                criaderos_a7: registro.a7,
                criaderos_a8: registro.a8,
                criaderos_a9: registro.a9,
                criaderos_a10: registro.a10,
                //fk_registro_s1_b: registro.id_cB,
                criaderos_b1: registro.b1,
                criaderos_b2: registro.b2,
                criaderos_b3: registro.b3,
                criaderos_b4: registro.b4,
                criaderos_b5: registro.b5,
                criaderos_b6: registro.b6,
                //fk_registro_s1_c: registro.id_cC,
                criaderos_c1: registro.c1,
                criaderos_c2: registro.c2,
                criaderos_c3: registro.c3,
                criaderos_c4: registro.c4,
                
            });
            
            //alert("Json :" + data);
                       
            $http.defaults.headers.post["Content-Type"] = "application/json";
            return $http.post(url, data, {}).then(
                function(success){
                    return success;
                }, function(error){
                    return error;
                });
            
        },
        
        listarPlanillas: function () {
            
            //$scope.respuesta = null;
            //var url = $rootScope.ip_servidor + "/movil/tablas1";
            var url = ip_servidor + "/movil/tablas1";
            var data = null;
            
            //$http.defaults.headers.post["Content-Type"] = "application/json";
            return $http.get(url, data, {}).then(
                function(success){
                    return success;
                }, function(error){
                    return error;
                });
            
        },
        
        buscarPlanilla: function (id_s1) {
            
            //$scope.respuesta = null;
            //var url = $rootScope.ip_servidor + "/movil/tablas1/" + id_s1;
            var url = ip_servidor + "/movil/tablas1/" + id_s1;
            var data = null;
            
            //$http.defaults.headers.post["Content-Type"] = "application/json";
            return $http.get(url, data, {}).then(
                function(success){
                    return success;
                }, function(error){
                    return error;
                });
            
        },
        
        modificarPlanilla: function (planilla) {
            
            //$scope.respuesta = null;
            //var url = $rootScope.ip_servidor + "/movil/tablas1/modificar";
            var url = ip_servidor + "/movil/tablas1/modificar";
            var data = JSON.stringify({
                id: planilla.id_server,
                fk_tipo_control: planilla.tipo_control.id,
                fk_barrio: planilla.barrio.id,
                //fk_distrito: planilla.distrito.id,
                //fk_departamento: planilla.barrio.id,
                fk_encargado: planilla.encargado.id,
                fk_supervisor: planilla.supervisor.id,
                fk_operador: planilla.operador.id,
                cuadrilla: planilla.cuadrilla,
                brigada: planilla.brigada,
                fk_usuario: planilla.usuario
            });
            //alert("modificar s1 service: " + JSON.stringify(data));
            $http.defaults.headers.post["Content-Type"] = "application/json";
            return $http.post(url, data, {}).then(
                function(success){
                    return success;
                }, function(error){
                    return error;
                });
            
        },
        
        modificarRegistro: function (registro) {         
            //$scope.respuesta = null;
            //var url = $rootScope.ip_servidor + "/movil/registro/modificar";
            var url = ip_servidor + "/movil/registro/modificar";
            var data = JSON.stringify({ 
                id : registro.id_server,
                fk_tabla_s1: registro.id_S1,
                manzana: registro.manzana,
                direccion: registro.direccion,
                //latitud: registro.latitude,
                //longitud: registro.longitude,
                //precision: registro.precision,
                //altitud: registro.altitud,
                verificado: registro.verificado,
                fk_tipo_predio: registro.predio.id,
                jefe_familia: registro.jefe_familia,
                habitantes: registro.habitantes,
                febriles: registro.febriles,
                sin_criaderos: registro.sin_criaderos,
                criaderos_eliminados: registro.criaderos_eliminados,
                tratados_fisicamente: registro.criaderos_tratados_fisicamente,
                tratados_larvicida: registro.criaderos_tratados_larvicida,
                larvicida_a: registro.larvicida_A,
                //larvicida_b: registro.larvicida_B,
                //larvicida_c: registro.larvicida_C,
                //fk_registro_s1_a: registro.id_cA,
                criaderos_a1: registro.a1,
                criaderos_a2: registro.a2,
                criaderos_a3: registro.a3,
                criaderos_a4: registro.a4,
                criaderos_a5: registro.a5,
                criaderos_a6: registro.a6,
                criaderos_a7: registro.a7,
                criaderos_a8: registro.a8,
                criaderos_a9: registro.a9,
                criaderos_a10: registro.a10,
                //fk_registro_s1_b: registro.id_cB,
                criaderos_b1: registro.b1,
                criaderos_b2: registro.b2,
                criaderos_b3: registro.b3,
                criaderos_b4: registro.b4,
                criaderos_b5: registro.b5,
                criaderos_b6: registro.b6,
                //fk_registro_s1_c: registro.id_cC,
                criaderos_c1: registro.c1,
                criaderos_c2: registro.c2,
                criaderos_c3: registro.c3,
                criaderos_c4: registro.c4,
                
            });
            
            //alert("Json :" + data);
                       
            $http.defaults.headers.post["Content-Type"] = "application/json";
            return $http.post(url, data, {}).then(
                function(success){
                    return success;
                }, function(error){
                    return error;
                });
            
        },
        
        cambiarContrasena: function (password) {
            
            //var url = "http://192.168.43.227:8080/movil/login";
            //var url = $rootScope.ip_servidor + "/movil/contrasena";
            var url = ip_servidor + "/movil/contrasena";
            var data = JSON.stringify({
                id_usuario: password.login,
                passanterior: password.actual,
                passnueva: password.nuevo1,
                passnueva2: password.nuevo2
            });
            
            $http.defaults.headers.post["Content-Type"] = "application/json";
            return $http.post(url, data, {}).then(
                function(success){
                    /*if (success.data['respuesta'] == true)
                        $location.path('/tab/operaciones');
                    else                        
                        alert("Login Incorrecto");*/
                    return success;
                }, function(error){
                    return error;   
                });
            
        },

    };
})
