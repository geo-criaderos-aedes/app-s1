Para poder generar la apk, se deben ejecutar los siguientes comandos:

```
sudo apt install npm
```

```
sudo npm install -g ionic@1.2.4
sudo npm install -g cordova@6.1.1
```

```
cd sources/geo-criaderos-aedes
ionic start app-s1 --type ionic1
cd app-s1
```

```
cordova plugin add cordova-plugin-geolocation
cordova plugin add cordova-plugin-file
cordova plugin add cordova-plugin-network-information
```


```
cordova platform add android
ionic build --release android
```
